class archer_save:
    def __init__(self):
        print('save file')

    def save_vtk(self,name,nx,ny,nz,x,y,z,names,*args):
        # routine to save archer data in vtk.
        # names vector of names of variables that will be passed
        # as *args
        with open(name, "w") as vtkFile:
         print('# vtk DataFile Version 2.0',file=vtkFile)
         print(name,file=vtkFile)
         print('ASCII' , file=vtkFile)
         print('DATASET RECTILINEAR_GRID', file=vtkFile)
         print('DIMENSIONS',nx,' ',ny,' ',nz,file=vtkFile)
         print('X_COORDINATES',nx,'float',file=vtkFile)
         for i in range(nx):
           print( x[i], file=vtkFile, sep=' ' )
         print('Y_COORDINATES',ny,'float',file=vtkFile)
         for j in range(ny):
           print( y[j], file=vtkFile, sep=' ' )
         print('Z_COORDINATES',nz,'float',file=vtkFile)
         for k in range(nz):
           print( z[k], file=vtkFile, sep=' ' )
         print('POINT_DATA ', (nx)*(ny)*(nz),file=vtkFile)
         index=0
         for ciao in args:
          print('SCALARS %s float 1'%(names[index]),file=vtkFile)
          print('LOOKUP_TABLE default',file=vtkFile)
          for (i,j,k) in [(i,j,k) for k in  range(nz) for j in range(ny) for i in range(nx) ]:
            print(ciao[i,j,k], file=vtkFile, sep=' ')
          index=index+1
        print('<3 vtk <3')


    def save_txt(self,name,x,y):
        # routine to save archer data in vtk.
        # names vector of names of variables that will be passed
        # as *args
        with open(name, "w") as txtFile:
         for i in range(len(x)):
            print( x[i], y[i], file=txtFile, sep=' ' )
        print('<3 txt <3')


    def save_vtk2(self,name,ny,nz,y,z,names,*args):
        # routine to save archer data in vtk.
        # names vector of names of variables that will be passed
        # as *args
        with open(name, "w") as vtkFile:
         print('# vtk DataFile Version 2.0',file=vtkFile)
         print(name,file=vtkFile)
         print('ASCII' , file=vtkFile)
         print('DATASET RECTILINEAR_GRID', file=vtkFile)
         print('DIMENSIONS',ny,' ',nz,file=vtkFile)
         print('Y_COORDINATES',ny,'float',file=vtkFile)
         for j in range(ny):
           print( y[j], file=vtkFile, sep=' ' )
         print('Z_COORDINATES',nz,'float',file=vtkFile)
         for k in range(nz):
           print( z[k], file=vtkFile, sep=' ' )
         print('POINT_DATA ',(ny)*(nz),file=vtkFile)
         index=0
         for ciao in args:
          print('SCALARS %s float 1'%(names[index]),file=vtkFile)
          print('LOOKUP_TABLE default',file=vtkFile)
          for (j,k) in [(j,k) for k in  range(nz) for j in range(ny)]:
            print(ciao[j,k], file=vtkFile, sep=' ')
          index=index+1
        print('<3 vtk <3')


