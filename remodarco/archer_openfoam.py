import numpy as np
import vtk
from vtk.util.numpy_support import vtk_to_numpy
import os
from remodarco.archer_plot import archer_plot as aplot
from remodarco.archer_file import archer_file as caso
from scipy.interpolate import griddata as gd
#------------------------------

class archer_openfoam(caso,aplot):
    def __init__(self,casefile,h5name):
       # super(archer_average, self).__init__(casefilename,h5name)
        super().__init__(casefile,h5name)
        self.path = "./openfoam/"

    def read_vtk(self,path_openfoam,variable):
        self.read_case() #to get the number of nodes
        reader=vtk.vtkUnstructuredGridReader()
        reader.SetFileName(self.path+path_openfoam)
        reader.ReadAllScalarsOn()
        reader.ReadAllVectorsOn()
        reader.ReadAllTensorsOn()
        reader.Update()
        vtkdata = reader.GetOutput()
        # get nodes in vtk
        nodes_vtk_array= reader.GetOutput().GetPoints().GetData()
        pointData = vtkdata.GetPointData().GetScalars(variable)
        #nodes in numpy
        nodes_nummpy_array = vtk_to_numpy(nodes_vtk_array) 
        x,y,z= nodes_nummpy_array[:,0] , nodes_nummpy_array[:,1] , nodes_nummpy_array[:,2]
        pointData_numpy = vtk_to_numpy(pointData)
        #generate new grid X,Y,Z
        print("Generate new grid...")
        xi,yi,zi=np.ogrid[0:1:150j, 0:1:150j, 0:1:150j]
        X1=xi.reshape(xi.shape[0],)
        Y1=yi.reshape(yi.shape[1],)
        Z1=zi.reshape(zi.shape[2],)
        ar_len=len(X1)*len(Y1)*len(Z1)
        X=np.arange(ar_len,dtype=float)
        Y=np.arange(ar_len,dtype=float)
        Z=np.arange(ar_len,dtype=float)
        l=0
        for i in range(0,len(X1)):
                for j in range(0,len(Y1)):
                            for k in range(0,len(Z1)):
                                X[l]=X1[i]
                                Y[l]=Y1[j]
                                Z[l]=Z1[k]
                                l=l+1
                                print("")
        point_data_interp = gd((x,y,z), pointData_numpy, (X,Y,Z), method='linear')
        arr_3d = point_data_interp.reshape((len(X1),len(Y1),len(Z1))).transpose()
        return X1,Y1,Z1,arr_3d

 
