import numpy as np
import matplotlib.pyplot as plt
import os
import numpy as np
import matplotlib.tri as tri
import matplotlib.cm as cm
import matplotlib.ticker


class archer_av_paraview:
    def __init__(self):
        print("average not archer")
        self.ngridx=4000
        self.ngridz=4000
        self.xi=np.linspace(0, 0.08, self.ngridx)
        self.zi=np.linspace(-0.04, 0.04, self.ngridz) 

    def interpolate_txt(self,file_name):
        D_a,X,Y,Z,p1,p2,rho1,rho2,T1,T2,ux1,ux2,uy1,uy2,uz1,uz2,VF1,VF2,Vaverage,liqdispav,uxguxg,uxluxl,uzguzg,uzluzl=np.genfromtxt(file_name,delimiter=',',unpack='True',skip_header=1)
        VF2_average = VF2
        Wg_average=ux1
        Vg_average=uz1
        Wl_average=ux2
        Vl_average=uz2
        WgWg_average = uxguxg-Wg_average*Wg_average
        VgVg_average = uzguzg-Vg_average*Vg_average
        WlWl_average = uxluxl-Wl_average*Wl_average
        VlVl_average = uzluzl-Vl_average*Vl_average
        DATA=VF2
        DATA4 = Wg_average
        DATA5 = Vg_average
        DATA7 = Wl_average
        DATA8 = Vl_average
        DATA9 = WgWg_average 
        DATA10 = VgVg_average 
        DATA11 = WlWl_average 
        DATA12 = VlVl_average
        DATA13 = liqdispav
        triang = tri.Triangulation(X, Z)
        interpolator = tri.LinearTriInterpolator(triang, DATA)
        interpolator4 = tri.LinearTriInterpolator(triang, DATA4)
        interpolator5 = tri.LinearTriInterpolator(triang, DATA5)
        interpolator7 = tri.LinearTriInterpolator(triang, DATA7)
        interpolator8 = tri.LinearTriInterpolator(triang, DATA8)
        interpolator9 = tri.LinearTriInterpolator(triang, DATA9)
        interpolator10 = tri.LinearTriInterpolator(triang, DATA10)
        interpolator11 = tri.LinearTriInterpolator(triang, DATA11)
        interpolator12 = tri.LinearTriInterpolator(triang, DATA12)
        interpolator13 = tri.LinearTriInterpolator(triang, DATA13)
        Xi, Zi = np.meshgrid(self.xi,self. zi)
        plotVOF = interpolator(Xi, Zi)
        plotWg = interpolator4(Xi,Zi)
        plotVg = interpolator5(Xi,Zi)
        plotWl = interpolator7(Xi,Zi)
        plotVl = interpolator8(Xi,Zi)
        plotWgWg = interpolator9(Xi,Zi)
        plotVgVg = interpolator10(Xi,Zi)
        plotWlWl = interpolator11(Xi,Zi)
        plotVlVl = interpolator12(Xi,Zi)
        plotliqdisp = interpolator13(Xi,Zi)
        return(plotVOF,plotWg,plotVg,plotWl,plotVl,plotWgWg,plotVgVg,plotWlWl,plotVlVl,plotliqdisp)
 



