import numpy as np
import h5py
from h5py import File as H5File
import os
import matplotlib.pyplot as plt
from remodarco.archer_save import archer_save as sauf

#------------------------------
class archer_file:
    pass
    def __init__(self,casefilename,h5name):
        self.casefilename = casefilename
        self.h5name = h5name
        self.nx = 0
        self.ny = 0
        self.nz = 0

    def read_case(self):

        """Read the '*.cas' input file of ARCHER"""

        for line in open(self.casefilename,"r"):
            if "IMAX" in line: self.nx = int(line.split("=")[1])
            if "JMAX" in line: self.ny = int(line.split("=")[1])
            if "KMAX" in line: self.nz = int(line.split("=")[1])
            if "NB_PROC_X" in line: self.npx = int(line.split("=")[1])
            if "NB_PROC_Y" in line: self.npy = int(line.split("=")[1])
            if "NB_PROC_Z" in line: self.npz = int(line.split("=")[1])
            if "LX" in line: self.Lx = float(line.split("=")[1])
        self.nproc = self.npx*self.npy*self.npz
        self.deltax=self.Lx/self.nx
        return self.nx, self.ny, self.nz, self.Lx/self.nz, self.npx, self.npy, self.npz,self.deltax

    def extract_time(self):        
        print("writing time file")
        xmf_file = []
        for file in os.listdir("."):
            if file.endswith(".xmf"):
                #ciao is a dummy variable to identify the different file that are the xfm tree files
                # and not the processor file, where there is written the time. It is identified by the length of the string that describe the file
                ciao=os.path.splitext(file)
                if len(ciao[0].split('_'))<4 and len(ciao[0].split('_'))>2 :
                    xmf_file.append(ciao[0])
                    # these files are appended to the vector xmf file declared abovve
        time_list = []
        for xmf in xmf_file:
            for line in open(xmf+".xmf","r"):
                if "<Time" in line:
                    #The " split the time
                    time = line.split('"')[1]
                    #creating a list of time
                    time_list.append(time)
        #sort the time list
        
        sorted_time = sorted(time_list,key=float) 
        
        #write a tmp file with the time list. Here there will be double time, because of the relaunch xmf

        with open('tmp.txt',"w") as time_file:
            for i in range(len(sorted_time)): print(sorted_time[i],file=time_file)
        lines_seen = set() # holds lines already seen

        #create the real time file. it is free by the time 

        outfile = open('time_list.txt', "w")
        for line in open('tmp.txt', "r"):
            if line not in lines_seen: # not a duplicate
                outfile.write(line)
                lines_seen.add(line)
        outfile.close()
        os.remove("tmp.txt")
           
        #import time as an object array for the class

        self.time = np.genfromtxt("time_list.txt")


    def catch_sequence(self):
        print("Catching sequence")
        xmf_file = []
        for file in os.listdir("."):
            if file.endswith(".xmf"):
                #ciao is a dummy variable to identify the different file that are the xfm tree files
                # and not the processor file
                ciao=os.path.splitext(file)
                if len(ciao[0].split('_'))<4 and len(ciao[0].split('_'))>2 :
                    xmf_file.append(ciao[0])
        self.relaunch=[]
        self.iteraz=[]
        relaunch_doub=[]
        iteraz_doub=[]
        for i in range(len(xmf_file)):
            relaunchtmp = xmf_file[i].split('_')[1]
            iteraztmp = xmf_file[i].split('_')[2]
            relaunch_doub.append(relaunchtmp)
            iteraz_doub.append(iteraztmp)
            #synced reordering
        iteraz_doub, relaunch_doub = (list(t) for t in zip(*sorted(zip(iteraz_doub, relaunch_doub))))
        iterazold='st00001'
        for i in range(len(iteraz_doub)):
            if (iteraz_doub[i]==iterazold):
                print("skipped %s, relaunch step"%iterazold)
            else:
                self.relaunch.append(relaunch_doub[i])
                self.iteraz.append(iteraz_doub[i])
                iterazold = iteraz_doub[i]
        return(self.relaunch,self.iteraz)


    def read_h5_scalar(self,relaunch,iteraz,name):
        print("read %s relaunch %s at time %s"%(name,relaunch,iteraz))
        pippo = np.zeros( (self.nx, self.ny, self.nz) )
        for proc in range(self.nproc):
            if isinstance(relaunch, str):
                filename = self.h5name+relaunch+ "_"+iteraz+"_p"+str(proc).zfill(6) + ".h5"
            else:
                filename = self.h5name+str(relaunch).zfill(5)+ "_st"+str(iteraz).zfill(6)+"_p"+str(proc).zfill(6) + ".h5"
            variab ="EULERIEN/"+name
            #filename = self.h5name+relaunch+ "_"+iteraz+"_p"+str(proc).zfill(6) + ".h5"
            fileh5 = h5py.File(filename,"r")
            datapippo = np.array(fileh5[variab])
            iz = proc % self.npz
            iy = (proc // self.npz) % self.npy
            ix = (proc // (self.npz * self.npy)) % self.npx

            pippo[ix * self.nx // self.npx: (ix+1) * self.nx // self.npx, \
               iy * self.ny // self.npy: (iy+1) * self.ny // self.npy, \
               iz * self.nz // self.npz: (iz+1) * self.nz // self.npz  ] = \
               np.moveaxis(datapippo[1:-1, 1:-1, 1:-1], [2, 0], [0, 2])
            fileh5.close()
        return pippo[:,:,::-1] #change direction of z axis for the initialization pupose



    def read_h5_vel(self,relaunch,iteraz):
        print("read vel relaunch %s at time %s"%(relaunch,iteraz))
        vel = np.zeros( (self.nx, self.ny, self.nz,3) )
        for proc in range(self.nproc):
            if isinstance(relaunch, str):
                filename = self.h5name+relaunch+ "_"+iteraz+"_p"+str(proc).zfill(6) + ".h5"
            else:
                filename = self.h5name+str(relaunch).zfill(5)+ "_st"+str(iteraz).zfill(6)+"_p"+str(proc).zfill(6) + ".h5"
           
            #filename = self.h5name+relaunch+ "_"+iteraz+"_p"+str(proc).zfill(6) + ".h5"
            fileh5 = h5py.File(filename,"r")
            datavel = np.array(fileh5["EULERIEN/velocity"])
            iz = proc % self.npz
            iy = (proc // self.npz) % self.npy
            ix = (proc // (self.npz * self.npy)) % self.npx

            vel[ix * self.nx // self.npx: (ix+1) * self.nx // self.npx, \
               iy * self.ny // self.npy: (iy+1) * self.ny // self.npy, \
               iz * self.nz // self.npz: (iz+1) * self.nz // self.npz, : ] = \
               np.moveaxis(datavel[1:-1, 1:-1, 1:-1], [2, 0], [0, 2])
            fileh5.close()
        return vel[:,:,::-1]

    def print_vof(self,z,y,start,end,partenza):
        dirName='frequenza'
        try:
            # Create target Directory
            os.mkdir(dirName)
            print("Directory " , dirName ,  " Created ")
        except FileExistsError:
            print("Directory " , dirName ,  " already exists")
        for seq in range(0,len(self.relaunch)):
          print(int(self.relaunch[seq]))
          if (int(self.relaunch[seq])>partenza and self.time[seq]>start and self.time[seq]<end):
           print("elaboro caso: %d"%seq)
           vof = self.read_h5_scalar(self.relaunch[seq],self.iteraz[seq],'vof')
           vof_print=vof
           fig=plt.figure()
           plt.contourf(z,y,vof_print[self.nx//2,:,:],levels=np.linspace(0,1,200),cmap='binary')
           plt.axis('equal')
           plt.xlim(0,16)
           plt.ylim(-8,8)
           plt.savefig(dirName+"/catch_freq_%d_%d.png"%(seq,self.time[seq]))
           plt.close(fig)
        else:
           print("skip caso seq %s iter %s"%(self.relaunch[seq],self.iteraz[seq]))



