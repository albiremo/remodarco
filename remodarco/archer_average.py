import numpy as np
import h5py
from h5py import File as H5File
import os
import matplotlib.pyplot as plt
from remodarco.archer_file import archer_file as caso
from remodarco.archer_plot import archer_plot as aplot
#------------------------------

class archer_average(caso,aplot):
    def __init__(self,casefilename,h5name):
       # super(archer_average, self).__init__(casefilename,h5name)
        self.path = "./figure/" 
        #super().__init__(casefilename,h5name)
        aplot.__init__(self)
        super().__init__(casefilename,h5name)
    def time_average(self,x,y,z,print_interm,start,name):
        var_aide= np.zeros( (self.nx, self.ny, self.nz) )
        timeold = self.time[0] #number of relaunch
        #timestart=4.7226E-03
        # in the loop is controlled if the number of the relaunch is equal to the previous
        # it means that we are at the last iteraz of the sequence, so it is the average we want
        for i in range(len(self.relaunch)-1):
         if self.relaunch[i] in start:
            print('skip %s relaunch'%(self.relaunch[i]))
         else:
            timestart = self.time[i-1]
            timeold= self.time[i-1]
            print('start time =', timestart)
            break

        for i in range(len(self.relaunch)-1):
          if self.relaunch[i] in start:
            print('skip %s relaunch'%(self.relaunch[i]))
          else:
            if self.relaunch[i]==self.relaunch[i+1]:
                print("skip %s %s"%(self.relaunch[i],self.iteraz[i]))
            else:
                print("average %s at time %s"%(self.relaunch[i],self.iteraz[i]))
                average_variable = self.read_h5_scalar(self.relaunch[i],self.iteraz[i],name)
                # vof_average collect all the vof_mean of the last iteraz
                var_aide += average_variable*(self.time[i]-timeold)
                if print_interm==1:
                        convergence=abs(var_aide[:,0:self.ny//2,:]/(self.time[i]-timestart)-np.flip(var_aide,1)[:,0:self.ny//2,:]/(self.time[i]-timestart))
                        name_save="converg_"+name+"_"+self.relaunch[i]+"_"+self.iteraz[i]
                        self.disegna_no_axis_complete(z,y[0:self.ny//2],convergence[self.nx//2,:,:],200,self.path,name_save,0,0.1)
                print(self.time[i]-timeold)
                timeold =self.time[i]
                # print of different average of the last time step
                if print_interm==1:
                    name_save="average_"+name+"_"+self.relaunch[i]+"_"+self.iteraz[i]
                    if name == "Vof_mean":
                        self.disegna_vof(z,y,average_variable[self.nx//2,:,:],self.path,name_save)
                    else:
                        self.disegna_general(z,y,average_variable[self.nx//2,:,:],200,self.path,name_save,0,0)
        #last add of average
        print("average %s at time %s"%(self.relaunch[-1],self.iteraz[-1]))
        average_variable = self.read_h5_scalar(self.relaunch[-1],self.iteraz[-1],name)
        if print_interm==1:
           name_save="average_"+name+"_"+self.relaunch[-1]+"_"+self.iteraz[-1]
           if name == "Vof_mean":
                self.disegna_vof(z,y,average_variable[self.nx//2,:,:],self.path,name_save)
           else:
                self.disegna_general(z,y,average_variable[self.nx//2,:,:],200,self.path,name_save,0,0)
        var_aide += average_variable*(self.time[-1]-timeold)
        if print_interm==1:
                        convergence=abs(var_aide[:,0:self.ny//2,:]/(self.time[-1]-timestart)-np.flip(var_aide,1)[:,0:self.ny//2,:]/(self.time[-1]-timestart))
                        name_save="converg_"+name+"_"+self.relaunch[-1]+"_"+self.iteraz[-1]
                        self.disegna_no_axis_complete(z,y[0:self.ny//2],convergence[self.nx//2,:,:],200,self.path,name_save,0,0.1)

        print(self.time[-1]-timeold)
        average_variable = var_aide/(self.time[-1]-timestart)
        return average_variable


    def space_average(self,field):
        #this routine returns the space average in x direction
        field_new = np.zeros( (self.ny,self.nz) )
        for i in range(self.nx):
            field_new += field[i,:,:]
        return field_new/self.nx

    def get_index(self,vector,dist):
        pos = 0
        for i in range(len(vector)):
            #print(vector[i],dist)
            if vector[i]>dist: 
                pos = i
                print(vector[i],pos)
                break
        return pos


    def extract_pdf(self,x,y,z,start,coord,windowy,windowz):
        dirName='curvature'
        try:
            # Create target Directory
            os.mkdir(dirName)
            print("Directory " , dirName ,  " Created ")
        except FileExistsError:
            print("Directory " , dirName ,  " already exists")
        z_shift=z+0.008
        y_shift=y-0.008
        #definition window where to analyze data
        posz=self.get_index(z_shift,coord)  
        posy=self.get_index(y_shift,0)
        deltay=10
        deltaz=10
        #deltay=int(round(len(y)/2)*windowy))
        #deltaz=int(round(len(z)/2*windowz))
        miny=posy-deltay; minz=posz-deltaz; maxy=posy+deltay; maxz=posz+deltaz
    #reading data
        k1_m=self.space_average(self.time_average(x,y,z,0,start,"kappa1med"))
        k2_m=self.space_average(self.time_average(x,y,z,0,start,"kappa2med"))
    # draw box
        print(minz,maxz,miny,maxy)
        vof_space = self.space_average(self.time_average(x,y,z,0,start,"Vof_mean"))   
        self.disegna_box(z,y,vof_space,200,self.path,"box_curv",0,1,z_shift[minz],y_shift[miny],z_shift[maxz]-z_shift[minz],y_shift[maxy]-y_shift[miny])
    #reshape data
        k1_temp = k1_m[miny:maxy,minz:maxz].reshape((maxy-miny*maxz-minz,1))
        k2_temp = k2_m[miny:maxy,minz:maxz].reshape((maxy-miny*maxz-minz,1))       
        rowk1,colsk1 = np.nonzero(k1_temp)
        rowk2,colsk2 = np.nonzero(k2_temp)
        k1_box=k1_temp[rowk1,colsk1]
        k2_box=k2_temp[rowk2,colsk2]
    #definition IRQ
        IRQ=2/(self.deltax*(k1_box+k2_box))
        fig=plt.figure()
        n,bins,patches=plt.hist(IRQ,bins=500,range=(-100,100),facecolor='blue')
        plt.axvline(2, color='k', linestyle='dashed', linewidth=1)
        plt.axvline(-2, color='k', linestyle='dashed', linewidth=1)
        plt.axvline(0, color='k', linestyle='solid', linewidth=1)
        plt.xlim(-25,25)
        plt.xlabel(r'$IRQ_k$')
        bin1_1=self.get_index(bins,-25)
        bin2_1=self.get_index(bins,-2)
        bin2_2=self.get_index(bins,2)
        bin3_2=self.get_index(bins,25)
        integral1 = sum(np.diff(bins)[bin1_1:bin2_1]*n[bin1_1:bin2_1]) 
        integral2 = sum(np.diff(bins)[bin2_1:bin2_2]*n[bin2_1:bin2_2]) 
        integral3 = sum(np.diff(bins)[bin2_2:bin3_2]*n[bin2_2:bin3_2]) 
        integral_tot=integral1+integral2+integral3
        perc1=integral1/integral_tot
        perc2=integral2/integral_tot
        perc3=integral3/integral_tot
        print('perc1=%f,perc2=%f,perc3+%f'%(perc1,perc2,perc3))
        plt.text(0,250,"%f"%perc2,ha="center")
        plt.text(-10,250,"%f"%perc1,ha="center")
        plt.text(10,250,"%f"%perc3,ha="center")
        plt.savefig("./curvature/IRQ.png")
        plt.close(fig)

