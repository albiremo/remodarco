import matplotlib.pyplot as plt
import matplotlib.patches as patches
import numpy as np

class archer_plot:
    def __init__(self):
        self.levels = np.linspace(0,1,200) #number of bins for VOF print
        self.ticchi=[0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1]
        self.scale=0.001 #dimension of the injector !!!! CHANGE IT IN CASE OF IS DIFFERENT !!!!
        self.map='binary'

    def disegna_vof(self,z,y,average_variable,path,name):
        fig = plt.figure()
        cs=plt.contourf((z+0.008)/self.scale,(y-0.008)/self.scale,average_variable,levels=self.levels,cmap=self.map,extend='both')
        cbar=plt.colorbar()
        cbar.set_ticks(self.ticchi)
        plt.xlabel('z[-]')
        plt.ylabel('y[-]')
        plt.savefig(path+name+".png")
        plt.close(fig) 

    def disegna_general(self,z,y,average_variable,livelli,path,name,valmin,valmax):
        fig = plt.figure()
        if valmin==0 and valmax==0:
            cs=plt.contourf((z+0.008)/self.scale,(y-0.008)/self.scale,average_variable,levels=livelli,cmap=self.map,extend='both')
        else: 
            cs=plt.contourf((z+0.008)/self.scale,(y-0.008)/self.scale,average_variable,levels=np.linspace(valmin,valmax,livelli),cmap=self.map,extend='both')
        cbar=plt.colorbar()
        plt.xlabel('z[-]')
        plt.ylabel('y[-]')
        plt.savefig(path+name+".png")
        plt.close(fig) 

    def disegna_no_axis(self,z,y,average_variable,livelli,path,name,valmin,valmax):
        fig = plt.figure()
        if valmin==0 and valmax==0:
            cs=plt.contourf((z+0.008)/self.scale,(y-0.008)/self.scale,average_variable,levels=livelli,cmap=self.map,extend='both')
        else: 
            cs=plt.contourf((z+0.008)/self.scale,(y-0.008)/self.scale,average_variable,levels=np.linspace(valmin,valmax,livelli),cmap=self.map,extend='both')
        plt.xlim(0,4)
        plt.ylim(-2,2)
        plt.xticks([])
        plt.yticks([])
        plt.savefig(path+name+".png")
        plt.close(fig) 


    def disegna_vof_no_axis(self,z,y,average_variable,path,name):
        fig, ax15 = plt.subplots()                                                             
        cs=ax15.contourf((z+0.008)/self.scale,(y-0.008)/self.scale,average_variable,levels=self.levels,cmap=self.map,extend='both')
        cx= ax15.contour((z+0.008)/self.scale,(y-0.008)/self.scale,average_variable,levels=[0.03,0.06,0.08,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0],colors='r')                                   
        plt.xlim(0,4)
        plt.ylim(-2,2)
        plt.xticks([])
        plt.yticks([])
        plt.savefig(path+name+".png")
        plt.close(fig) 


    def disegna_box(self,z,y,average_variable,livelli,path,name,valmin,valmax,minz,maxz,miny,maxy):
        fig,cs = plt.subplots()
        if valmin==0 and valmax==0:
            img=cs.contourf((z+0.008)/self.scale,(y-0.008)/self.scale,average_variable,levels=livelli,cmap=self.map,extend='both')
        else: 
            img=cs.contourf((z+0.008)/self.scale,(y-0.008)/self.scale,average_variable,levels=np.linspace(valmin,valmax,livelli),cmap=self.map,extend='both')
        cs.add_patch(
           patches.Rectangle(
           (minz/self.scale, maxz/self.scale),
            miny/self.scale,
            maxy/self.scale,
            fill=False      # remove background
        )) 
        plt.colorbar(img)
        plt.xlabel('z[-]')
        plt.ylabel('y[-]')
        plt.savefig(path+name+".png")
        plt.close(fig) 

    def disegna_no_axis_complete(self,z,y,average_variable,livelli,path,name,valmin,valmax):
        fig = plt.figure()
        if valmin==0 and valmax==0:
            cs=plt.contourf((z+0.008)/self.scale,(y-0.008)/self.scale,average_variable,levels=livelli,cmap=self.map,extend='both')
        else: 
            cs=plt.contourf((z+0.008)/self.scale,(y-0.008)/self.scale,average_variable,levels=np.linspace(valmin,valmax,livelli),cmap=self.map,extend='both')
        plt.xlim(0,16)
        plt.ylim(-8,0)
        plt.xticks([])
        plt.yticks([])
        plt.savefig(path+name+".png")
        plt.close(fig) 


