#import os
#os.environ["PATH"] += os.pathsep + '/home/2017004/aremig02/latex/bin/x86_64-linux'
#import matplotlib
# Force matplotlib to not use any Xwindows backend.
#matplotlib.use('Agg')
import os
import numpy as np
import matplotlib.tri as tri
import matplotlib.cm as cm
import matplotlib.ticker

from remodarco.archer_save import archer_save as sv
from remodarco.archer_plot import archer_plot as apl
from remodarco.archer_elab import archer_elab as el

import matplotlib.pyplot as plt
#---------------------------------------------------------
elab = el()
savage=sv()
drawing=apl()
#----------------------------------------------------------

print_alpha=1
print_velocity=0
print_reynolds=0
alphav=0
#----------------------------------------------------------
class FormatScalarFormatter(matplotlib.ticker.ScalarFormatter):
        def __init__(self, fformat="%1.1f", offset=True, mathText=True):
                    self.fformat = fformat
                    matplotlib.ticker.ScalarFormatter.__init__(self,useOffset=offset,
                    useMathText=mathText)
        def _set_format(self, vmin, vmax):
                     self.format = self.fformat
                     if self._useMathText:
                      self.format = '$%s$' % matplotlib.ticker._mathdefault(self.format)
def view_colormap(cmap):
            cmap = plt.cm.get_cmap(cmap)
            colors = cmap(np.arange(cmap.N))                                
            fig, ax = plt.subplots()
            subplot_kw=dict(xticks=[], yticks=[])
            plt.imsave("gray.png",[colors])
######################################################################
dirName1='figure'
dirName2='txt'
try:
    # Create target Directory
    os.mkdir(dirName1)
    print("Directory " , dirName1 ,  " Created ")
except FileExistsError:
    print("Directory " , dirName1 ,  " already exists")

try:
    # Create target Directory
    os.mkdir(dirName2)
    print("Directory " , dirName2 ,  " Created ")
except FileExistsError:
    print("Directory " , dirName2 ,  " already exists")




# interpolation
print("interpolate")
ngridy=4000
ngridz=4000

t0=0.002
t1=0.0038
t2=0.0055
t3=0.0072
t4=0.0088
t5=0.0104
X,Y,Z,Uxmean,Uymean,Uzmean,UxUx,UyUy,UzUz,UxUy,UxUz,UyUz,Ugxmean,Ugymean,Ugzmean,UgxUgx,UgUgMean1,UgUgMean2,UgUgMean3,UgUgy,UgUgMean5,UgUgMean6,UgUgMean7,UgzUgz,UlUlMean0,UlUlMean1,UlUlMean2,UlUlMean3,UlyUly,UlUlMean5,UlUlMean6,UlUlMean7,UlzUlz,VFav,alphaUMeanx,alphaUMeany,alphaUMeanz,p,pgh=np.genfromtxt("0_0038.csv",delimiter=',',unpack='True',skip_header=1)

X,Y,Z,Uxmean1,Uymean1,Uzmean1,UxUx1,UyUy1,UzUz1,UxUy1,UyUz1,UxUz1,Ugxmean1,Ugymean1,Ugzmean1,UgxUgx1,UgUgMean11,UgUgMean21,UgUgMean31,UgUgy1,UgUgMean51,UgUgMean61,UgUgMean71,UgzUgz1,UlUlMean01,UlUlMean11,UlUlMean21,UlUlMean31,UlyUly1,UlUlMean51,UlUlMean61,UlUlMean71,UlzUlz1,VFav1,alphaUMeanx1,alphaUMeany1,alphaUMeanz1,p,pgh1=np.genfromtxt("0_0055.csv",delimiter=',',unpack='True',skip_header=1)

X,Y,Z,Uxmean2,Uymean2,Uzmean2,UxUx2,UyUy2,UzUz2,UxUy2,UyUz2,UxUz2,Ugxmean2,Ugymean2,Ugzmean2,UgxUgx2,UgUgMean12,UgUgMean22,UgUgMean32,UgUgy2,UgUgMean52,UgUgMean62,UgUgMean72,UgzUgz2,UlUlMean02,UlUlMean12,UlUlMean22,UlUlMean32,UlyUly2,UlUlMean52,UlUlMean62,UlUlMean72,UlzUlz2,VFav2,alphaUMeanx2,alphaUMeany2,alphaUMeanz2,p,pgh2=np.genfromtxt("0_0072.csv",delimiter=',',unpack='True',skip_header=1)

X,Y,Z,Uxmean3,Uymean3,Uzmean3,UxUx3,UyUy3,UzUz3,UxUy3,UyUz3,UxUz3,Ugxmean3,Ugymean3,Ugzmean3,UgxUgx3,UgUgMean13,UgUgMean23,UgUgMean33,UgUgy3,UgUgMean53,UgUgMean63,UgUgMean73,UgzUgz3,UlUlMean03,UlUlMean13,UlUlMean23,UlUlMean33,UlyUly3,UlUlMean53,UlUlMean63,UlUlMean73,UlzUlz3,VFav3,alphaUMeanx3,alphaUMeany3,alphaUMeanz3,p3,pgh3=np.genfromtxt("0_0088.csv",delimiter=',',unpack='True',skip_header=1)


X,Y,Z,Uxmean4,Uymean4,Uzmean4,UxUx4,UyUy4,UzUz4,UxUy4,UyUz4,UxUz4,Ugxmean4,Ugymean4,Ugzmean4,UgxUgx4,UgUgMean14,UgUgMean24,UgUgMean34,UgUgy4,UgUgMean54,UgUgMean64,UgUgMean74,UgzUgz4,UlUlMean04,UlUlMean14,UlUlMean24,UlUlMean34,UlyUly4,UlUlMean54,UlUlMean64,UlUlMeanz4,UlzUlz4,VFav4,alphaUMeanx4,alphaUMeany4,alphaUMeanz4,p4,pgh4=np.genfromtxt("0_0104.csv",delimiter=',',unpack='True',skip_header=1)

VF2_average = (VFav*(t1-t0)+VFav1*(t2-t1)+VFav2*(t3-t2)+VFav3*(t4-t3)+VFav4*(t5-t4))/(t5-t0)
VZ2_average = (Uymean*(t1-t0)+Uymean1*(t2-t1)+Uymean2*(t3-t2)+Uymean3*(t4-t3)+Uymean4*(t5-t4))/(t5-t0)
VV2_average = (UyUy*(t1-t0)+UyUy1*(t2-t1)+UyUy2*(t3-t2)+UyUy3*(t4-t3)+UyUy4*(t5-t4))/(t5-t0)
ViVi_av=(UyUy*(t1-t0)+UyUy1*(t2-t1)+UyUy2*(t3-t2)+UyUy3*(t4-t3)+UyUy4*(t5-t4))/(t5-t0)#VV2_average#-VZ2_average*VZ2_average
VX2_average = (Uzmean*(t1-t0)+Uzmean1*(t2-t1)+Uzmean2*(t3-t2)+Uzmean3*(t4-t3)+Uzmean4*(t5-t4))/(t5-t0)
WW2_average = (UzUz*(t1-t0)+UzUz1*(t2-t1)+UzUz2*(t3-t2)+UzUz3*(t4-t3)+UzUz4*(t5-t4))/(t5-t0)
WiWi_av=(UzUz*(t1-t0)+UzUz1*(t2-t1)+UzUz2*(t3-t2)+UzUz3*(t4-t3)+UzUz4*(t5-t4))/(t5-t0)#WW2_average#-VX2_average*VX2_average
alphaw_average=(alphaUMeanz*(t1-t0)+alphaUMeanz1*(t2-t1)+alphaUMeanz2*(t3-t2)+alphaUMeanz3*(t4-t3)+alphaUMeanz4*(t5-t4))/(t5-t0)
alphav_average=(alphaUMeany*(t1-t0)+alphaUMeany1*(t2-t1)+alphaUMeany2*(t3-t2)+alphaUMeany3*(t4-t3)+alphaUMeany4*(t5-t4))/(t5-t0)
Wg_average=(Ugzmean*(t1-t0)+Ugzmean1*(t2-t1)+Ugzmean2*(t3-t2)+Ugzmean3*(t4-t3)+Ugzmean4*(t5-t4))/((t5-t0)*(1-VF2_average))
Vg_average=(Ugymean*(t1-t0)+Ugymean1*(t2-t1)+Ugymean2*(t3-t2)+Ugymean3*(t4-t3)+Ugymean4*(t5-t4))/((t5-t0)*(1-VF2_average))
aivi_av=alphav_average-VF2_average*VZ2_average
aiwi_av=alphaw_average-VF2_average*VZ2_average
Wli = alphaw_average/VF2_average
Vli = alphav_average/VF2_average
WgWg_average = ((UgzUgz*(t1-t0)+UgzUgz1*(t2-t1)+UgzUgz2*(t3-t2)+UgzUgz3*(t4-t3)+UgzUgz4*(t5-t4))/((t5-t0)*(1-VF2_average)))-Wg_average*Wg_average
VgVg_average = (UgUgy*(t1-t0)+UgUgy1*(t2-t1)+UgUgy2*(t3-t2)+UgUgy3*(t4-t3)+UgUgy4*(t5-t4))/((t5-t0)*(1-VF2_average))-Vg_average*Vg_average
WlWl_average = (UlzUlz*(t1-t0)+UlzUlz1*(t2-t1)+UlzUlz2*(t3-t2)+UlzUlz3*(t4-t3)+UlzUlz4*(t5-t4))/((t5-t0)*VF2_average)-Wli*Wli
VlVl_average = (UlyUly*(t1-t0)+UlyUly1*(t2-t1)+UlyUly2*(t3-t2)+UlyUly3*(t4-t3)+UlyUly4*(t5-t4))/((t5-t0)*VF2_average)-Vli*Vli
#---------------------------------------------------------------------------------------------------------------

DATA=VF2_average
DATA2 = ViVi_av
DATA3 = WiWi_av
DATA4 = VX2_average
DATA5 = VZ2_average
DATA6 = aivi_av
DATA7 = Wli
DATA8 = Vli
DATA9 = WgWg_average 
DATA10 = VgVg_average 
DATA11 = WlWl_average 
DATA12 = VlVl_average 
DATA13 = aiwi_av
DATA14  = Wg_average
DATA15 = Vg_average

zii = np.linspace(-0.008, 0.008, ngridz)
yii = np.linspace(0, 0.016, ngridy)
triang = tri.Triangulation(Z, Y)
interpolator = tri.LinearTriInterpolator(triang, DATA)
interpolator2 = tri.LinearTriInterpolator(triang, DATA2)
interpolator3 = tri.LinearTriInterpolator(triang, DATA3)
interpolator4 = tri.LinearTriInterpolator(triang, DATA4)
interpolator5 = tri.LinearTriInterpolator(triang, DATA5)
interpolator6 = tri.LinearTriInterpolator(triang, DATA6)
interpolator7 = tri.LinearTriInterpolator(triang, DATA7)
interpolator8 = tri.LinearTriInterpolator(triang, DATA8)
interpolator9 = tri.LinearTriInterpolator(triang, DATA9)
interpolator10 = tri.LinearTriInterpolator(triang, DATA10)
interpolator11 = tri.LinearTriInterpolator(triang, DATA11)
interpolator12 = tri.LinearTriInterpolator(triang, DATA12)
interpolator13 = tri.LinearTriInterpolator(triang, DATA13)
interpolator14 = tri.LinearTriInterpolator(triang, DATA14)
interpolator15 = tri.LinearTriInterpolator(triang, DATA15)


Xi, Zi = np.meshgrid(zii, yii)


DATAi = interpolator(Xi, Zi)
DATAvvi = interpolator2(Xi,Zi)
DATAwwi = interpolator3(Xi,Zi)
DATAwi = interpolator4(Xi,Zi)
DATAvi = interpolator5(Xi,Zi)
DATAalphavi = interpolator6(Xi,Zi)
DATAWl = interpolator7(Xi,Zi)
DATAVl = interpolator8(Xi,Zi)
DATAWgWg = interpolator9(Xi,Zi)
DATAVgVg = interpolator10(Xi,Zi)
DATAWlWl = interpolator11(Xi,Zi)
DATAVlVl = interpolator12(Xi,Zi)
DATAalphawi = interpolator13(Xi,Zi)
DATAWgi = interpolator14(Xi,Zi)
DATAVgi = interpolator15(Xi,Zi)

zi=zii+0.008
yi=yii-0.008

print("print figure")

plotVOF=DATAi[:,::-1]
plotVV=DATAvvi[:,::-1]
plotWW=DATAwwi[:,::-1]
plotW=-DATAwi[:,::-1]
plotV=DATAvi[:,::-1]
plotalphavi=DATAalphavi[:,::-1]
plotWl = np.nan_to_num(DATAWl[:,::-1]) 
plotVl = np.nan_to_num(DATAVl[:,::-1]) 
plotWgWg = np.nan_to_num(DATAWgWg[:,::-1]) 
plotVgVg = np.nan_to_num(DATAVgVg[:,::-1]) 
plotWlWl = np.nan_to_num(DATAWlWl[:,::-1]) 
plotVlVl = np.nan_to_num(DATAVlVl[:,::-1])
plotalphawi = DATAalphawi[:,::-1]
plotWg = np.nan_to_num(DATAWgi[:,::-1])
plotVg = np.nan_to_num(DATAVgi[:,::-1])
term1z=plotVOF*plotVlVl
term2z=(1-plotVOF)*plotVgVg
term3z=plotVOF*(1-plotVOF)*(plotVl-plotVg)**2
term1x=plotVOF*plotWlWl
term2x=(1-plotVOF)*plotWgWg
term3x=plotVOF*(1-plotVOF)*(plotWl-plotWg)**2
reconstry=plotVOF*plotVlVl+(1-plotVOF)*plotVgVg+plotVOF*(1-plotVOF)*(plotVl-plotVg)**2
reconstrz=plotVOF*plotWlWl+(1-plotVOF)*plotWgWg+plotVOF*(1-plotVOF)*(plotWl-plotWg)**2
liquid_disp=plotVOF*(1-plotVOF)
####################################################
x1=el.get_index(zi,0.001)
x2=el.get_index(zi,0.002)
x3=el.get_index(zi,0.003)
#############################################






##------------------------------------------------------------------------------------------
#view_colormap('binary')
##----------- figura angolo---------------------------------
if print_alpha==1:
 residual=plotVOF[0:ngridy//2,:]-np.flip(plotVOF,0)[0:ngridy//2,:]
 drawing.disegna_general(zii,yii[0:ngridy//2],np.absolute(residual),200,"./figure/","residual",0,0.05)
 drawing.disegna_no_axis(zii,yii[0:ngridy//2],np.absolute(residual),200,"./figure/","residual",0,0.05)
 drawing.disegna_vof(zii,yii,plotVOF,"./figure/","vof_average")
 drawing.disegna_vof_no_axis(zii,yii,plotVOF,"./figure/","vof_average_latex")
 drawing.disegna_no_axis(zii,yii,liquid_disp,200,"./figure/","liquid_disp",0,0.250)
 savage.save_txt('txt/line_vof_inter.txt',zi/0.001,plotVOF[ngridz//2,:])
 savage.save_txt('txt/line_vof_0_001_inter.txt',yi/0.001,plotVOF[:,x1])
 savage.save_txt('txt/line_vof_0_002_inter.txt',yi/0.001,plotVOF[:,x2])
 savage.save_txt('txt/line_vof_0_003_inter.txt',yi/0.001,plotVOF[:,x3])
 savage.save_txt('txt/line_liq_disp_0_001_inter.txt',yi/0.001,liquid_disp[:,x1])
 savage.save_txt('txt/line_liq_disp_0_002_inter.txt',yi/0.001,liquid_disp[:,x2])
 savage.save_txt('txt/line_liq_disp_0_003_inter.txt',yi/0.001,liquid_disp[:,x3])
if print_velocity==1:
 drawing.disegna_no_axis(zii,yii,plotW,200,"./figure/","ux",0,70)
 drawing.disegna_no_axis(zii,yii,plotV,200,"./figure/","uy",-3,3)
 savage.save_txt('txt/line_ux_0_001_inter.txt',yi/0.001,plotW[:,x1])
 savage.save_txt('txt/line_ux_0_002_inter.txt',yi/0.001,plotW[:,x2])
 savage.save_txt('txt/line_ux_0_003_inter.txt',yi/0.001,plotW[:,x3])
 savage.save_txt('txt/line_uz_0_001_inter.txt',yi/0.001,plotV[:,x1])
 savage.save_txt('txt/line_uz_0_002_inter.txt',yi/0.001,plotV[:,x2])
 savage.save_txt('txt/line_uz_0_003_inter.txt',yi/0.001,plotV[:,x3])
if print_reynolds==1:
 drawing.disegna_no_axis(zii,yii,plotWW,200,"./figure/","uxux",0,300)
 drawing.disegna_no_axis(zii,yii,plotVV,200,"./figure/","uzuz",0,350)
 savage.save_txt('txt/line_uxux_0_001_inter.txt',yi/0.001,plotWW[:,x1])
 savage.save_txt('txt/line_uxux_0_002_inter.txt',yi/0.001,plotWW[:,x2])
 savage.save_txt('txt/line_uxux_0_003_inter.txt',yi/0.001,plotWW[:,x3])
 savage.save_txt('txt/line_uzuz_0_001_inter.txt',yi/0.001,plotVV[:,x1])
 savage.save_txt('txt/line_uzuz_0_002_inter.txt',yi/0.001,plotVV[:,x2])
 savage.save_txt('txt/line_uzuz_0_003_inter.txt',yi/0.001,plotVV[:,x3])
if alphav==1:
 drawing.disegna_limited(zii,yii,plotWl-plotWg,200,"./figure/","W_diff",0,0)
 drawing.disegna_limited(zii,yii,plotVl-plotVg,200,"./figure/","V_diff",0,0)
 drawing.disegna_limited(zii,yii,plotWl,200,"./figure/","Wl",0,0)
 drawing.disegna_limited(zii,yii,plotalphawi,200,"./figure/","alphawi",0,0)
 drawing.disegna_no_axis(zii,yii,plotalphavi,200,"./figure/","alphavi",-1.5,1.5)
 drawing.disegna_limited(zii,yii,plotVl,200,"./figure/","Vl",0,0)
 drawing.disegna_no_axis(zii,yii,plotWgWg,200,"./figure/","wgwg",0,260)
 drawing.disegna_no_axis(zii,yii,plotVgVg,200,"./figure/","vgvg",0,330)
 drawing.disegna_no_axis(zii,yii,plotVlVl,200,"./figure/","vlvl",0,30)
 drawing.disegna_no_axis(zii,yii,plotWlWl,200,"./figure/","wlwl",0,30)
 drawing.disegna_no_axis(zii,yii,reconstry,200,"./figure/","reconstruct_y",0,300)
 drawing.disegna_no_axis(zii,yii,reconstrz,200,"./figure/","reconstruct_z",0,350)
 drawing.disegna_no_axis(zii,yii,term1z,200,"./figure/","term1z",0,3)
 drawing.disegna_no_axis(zii,yii,term2z,200,"./figure/","term2z",0,350)
 drawing.disegna_no_axis(zii,yii,term3z,200,"./figure/","term3z",0,4)
 drawing.disegna_no_axis(zii,yii,term1x,200,"./figure/","term1x",0,2.8)
 drawing.disegna_no_axis(zii,yii,term2x,200,"./figure/","term2x",0,350)
 drawing.disegna_no_axis(zii,yii,term3x,200,"./figure/","term3x",0,70)
 savage.save_txt('txt/line_alphav_0_001_inter.txt',yi/0.001,plotalphavi[:,x1])
 savage.save_txt('txt/line_alphav_0_002_inter.txt',yi/0.001,plotalphavi[:,x2])
 savage.save_txt('txt/line_alphav_0_003_inter.txt',yi/0.001,plotalphavi[:,x3])
 savage.save_txt('txt/line_uxguxg_0_001_inter.txt',yi/0.001,plotWgWg[:,x1])
 savage.save_txt('txt/line_uxguxg_0_002_inter.txt',yi/0.001,plotWgWg[:,x2])
 savage.save_txt('txt/line_uxguxg_0_003_inter.txt',yi/0.001,plotWgWg[:,x3])
 savage.save_txt('txt/line_uxluxl_0_001_inter.txt',yi/0.001,plotWlWl[:,x1])
 savage.save_txt('txt/line_uxluxl_0_002_inter.txt',yi/0.001,plotWlWl[:,x2])
 savage.save_txt('txt/line_uxluxl_0_003_inter.txt',yi/0.001,plotWlWl[:,x3])
 savage.save_txt('txt/line_uzguzg_0_001_inter.txt',yi/0.001,plotVgVg[:,x1])
 savage.save_txt('txt/line_uzguzg_0_002_inter.txt',yi/0.001,plotVgVg[:,x2])
 savage.save_txt('txt/line_uzguzg_0_003_inter.txt',yi/0.001,plotVgVg[:,x3])
 savage.save_txt('txt/line_uzluzl_0_001_inter.txt',yi/0.001,plotVlVl[:,x1])
 savage.save_txt('txt/line_uzluzl_0_002_inter.txt',yi/0.001,plotVlVl[:,x2])
 savage.save_txt('txt/line_uzluzl_0_003_inter.txt',yi/0.001,plotVlVl[:,x3])
 savage.save_txt('txt/line_reconstry_0_001_inter.txt',yi/0.001,reconstry[:,x1])
 savage.save_txt('txt/line_reconstry_0_002_inter.txt',yi/0.001,reconstry[:,x2])
 savage.save_txt('txt/line_reconstry_0_003_inter.txt',yi/0.001,reconstry[:,x3])
 savage.save_txt('txt/line_reconstrz_0_001_inter.txt',yi/0.001,reconstrz[:,x1])
 savage.save_txt('txt/line_reconstrz_0_002_inter.txt',yi/0.001,reconstrz[:,x2])
 savage.save_txt('txt/line_reconstrz_0_003_inter.txt',yi/0.001,reconstrz[:,x3])
 savage.save_txt('txt/line_ux_diff_0_001_inter.txt',yi/0.001,plotWl[:,x1]-plotWg[:,x1])
 savage.save_txt('txt/line_ux_diff_0_002_inter.txt',yi/0.001,plotWl[:,x2]-plotWg[:,x1])
 savage.save_txt('txt/line_ux_diff_0_003_inter.txt',yi/0.001,plotWl[:,x3]-plotWg[:,x3])
 savage.save_txt('txt/line_uz_diff_0_001_inter.txt',yi/0.001,plotVl[:,x1]-plotVg[:,x1])
 savage.save_txt('txt/line_uz_diff_0_002_inter.txt',yi/0.001,plotVl[:,x2]-plotVg[:,x1])
 savage.save_txt('txt/line_uz_diff_0_003_inter.txt',yi/0.001,plotVl[:,x3]-plotVg[:,x3])
 savage.save_txt('txt/line_term1z_0_001_inter.txt',yi/0.001,term1z[:,x1])
 savage.save_txt('txt/line_term1z_0_002_inter.txt',yi/0.001,term1z[:,x2])
 savage.save_txt('txt/line_term1z_0_003_inter.txt',yi/0.001,term1z[:,x3])
 savage.save_txt('txt/line_term2z_0_001_inter.txt',yi/0.001,term2z[:,x1])
 savage.save_txt('txt/line_term2z_0_002_inter.txt',yi/0.001,term2z[:,x2])
 savage.save_txt('txt/line_term2z_0_003_inter.txt',yi/0.001,term2z[:,x3])
 savage.save_txt('txt/line_term3z_0_001_inter.txt',yi/0.001,term3z[:,x1])
 savage.save_txt('txt/line_term3z_0_002_inter.txt',yi/0.001,term3z[:,x2])
 savage.save_txt('txt/line_term3z_0_003_inter.txt',yi/0.001,term3z[:,x3])
 savage.save_txt('txt/line_term1x_0_001_inter.txt',yi/0.001,term1x[:,x1])
 savage.save_txt('txt/line_term1x_0_002_inter.txt',yi/0.001,term1x[:,x2])
 savage.save_txt('txt/line_term1x_0_003_inter.txt',yi/0.001,term1x[:,x3])
 savage.save_txt('txt/line_term2x_0_001_inter.txt',yi/0.001,term2x[:,x1])
 savage.save_txt('txt/line_term2x_0_002_inter.txt',yi/0.001,term2x[:,x2])
 savage.save_txt('txt/line_term2x_0_003_inter.txt',yi/0.001,term2x[:,x3])
 savage.save_txt('txt/line_term3x_0_001_inter.txt',yi/0.001,term3x[:,x1])
 savage.save_txt('txt/line_term3x_0_002_inter.txt',yi/0.001,term3x[:,x2])
 savage.save_txt('txt/line_term3x_0_003_inter.txt',yi/0.001,term3x[:,x3])



