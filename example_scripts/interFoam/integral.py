#import os
#os.environ["PATH"] += os.pathsep + '/home/2017004/aremig02/latex/bin/x86_64-linux'
#import matplotlib
# Force matplotlib to not use any Xwindows backend.
#matplotlib.use('Agg')
import os
import numpy as np
import matplotlib.tri as tri
import matplotlib.cm as cm
import matplotlib.ticker

from remodarco.archer_save import archer_save as sv
from remodarco.archer_plot import archer_plot as apl
from remodarco.archer_elab import archer_elab as el

import matplotlib.pyplot as plt
#---------------------------------------------------------
elab = el()
savage=sv()
drawing=apl()
#----------------------------------------------------------
###########################################
tag_charg='_0_0030.csv' ##################### cambiare se si vuole cambiare time step ##########################
tag='0_003.png'    ##################### cambiare se si vuole cambiare time step ##########################
tag_txt='0_003'    ##################### cambiare se si vuole cambiare time step ##########################

print_alpha=1
print_velocity=1
print_reynolds=1
alphav=1
#----------------------------------------------------------
class FormatScalarFormatter(matplotlib.ticker.ScalarFormatter):
        def __init__(self, fformat="%1.1f", offset=True, mathText=True):
                    self.fformat = fformat
                    matplotlib.ticker.ScalarFormatter.__init__(self,useOffset=offset,
                    useMathText=mathText)
        def _set_format(self, vmin, vmax):
                     self.format = self.fformat
                     if self._useMathText:
                      self.format = '$%s$' % matplotlib.ticker._mathdefault(self.format)
def view_colormap(cmap):
            cmap = plt.cm.get_cmap(cmap)
            colors = cmap(np.arange(cmap.N))                                
            fig, ax = plt.subplots()
            subplot_kw=dict(xticks=[], yticks=[])
            plt.imsave("gray.png",[colors])
######################################################################
dirName1='figure_integral'
dirName2='txt_integral'
try:
    # Create target Directory
    os.mkdir(dirName1)
    print("Directory " , dirName1 ,  " Created ")
except FileExistsError:
    print("Directory " , dirName1 ,  " already exists")

try:
    # Create target Directory
    os.mkdir(dirName2)
    print("Directory " , dirName2 ,  " Created ")
except FileExistsError:
    print("Directory " , dirName2 ,  " already exists")




# interpolation
print("interpolate")
ngridx=1000
ngridy=4000

t0=0.0038
t1=0.0057
t2=0.0075
t3=0.008
t4=0.0099

Uxmean,Uymean,Uzmean,UxUx,UyUy,UzUz,UxUy,UxUz,UyUz,Ugxmean,Ugymean,Ugzmean,ugp0,ugp1,ugp2,ugp3,ugp4,ugp5,UgxUgx,UgUgMean1,UgUgMean2,UgUgMean3,UgUgy,UgUgMean5,UgUgMean6,UgUgMean7,UgzUgz,UlUlMean0,UlUlMean1,UlUlMean2,UlUlMean3,UlyUly,UlUlMean5,UlUlMean6,UlUlMean7,UlzUlz,VFav,VFavprime,alphaUMeanx,alphaUMeany,alphaUMeanz,dummy1,dummy2,dummy3,dummy4,dummy5,dummy6,alphaprime,alphaprimeprime,X,Y,Z=np.genfromtxt("0_0057"+tag_charg,delimiter=',',unpack='True',skip_header=1)

Uxmean1,Uymean1,Uzmean1,UxUx1,UyUy1,UzUz1,UxUy1,UxUz1,UyUz1,Ugxmean1,Ugymean1,Ugzmean1,ugp01,ugp11,ugp21,ugp31,ugp41,ugp51,UgxUgx1,UgUgMean11,UgUgMean21,UgUgMean31,UgUgy1,UgUgMean51,UgUgMean61,UgUgMean71,UgzUgz1,UlUlMean01,UlUlMean11,UlUlMean21,UlUlMean31,UlyUly1,UlUlMean51,UlUlMean61,UlUlMean71,UlzUlz1,VFav1,VFavprime1,alphaUMeanx1,alphaUMeany1,alphaUMeanz1,dummy11,dummy21,dummy31,dummy41,dummy51,dummy61,alphaprime1,alphaprimeprime1,X,Y,Z=np.genfromtxt("0_0057"+tag_charg,delimiter=',',unpack='True',skip_header=1)


Uxmean2,Uymean2,Uzmean2,UxUx2,UyUy2,UzUz2,UxUy2,UxUz2,UyUz2,Ugxmean2,Ugymean2,Ugzmean2,ugp02,ugp12,ugp22,ugp32,ugp42,ugp52,UgxUgx2,UgUgMean12,UgUgMean22,UgUgMean32,UgUgy2,UgUgMean52,UgUgMean62,UgUgMean72,UgzUgz2,UlUlMean02,UlUlMean12,UlUlMean22,UlUlMean32,UlyUly2,UlUlMean52,UlUlMean62,UlUlMean72,UlzUlz2,VFav2,VFavprime2,alphaUMeanx2,alphaUMeany2,alphaUMeanz2,dummy12,dummy22,dummy32,dummy42,dummy52,dummy62,alphaprime2,alphaprimeprime2,X,Y,Z=np.genfromtxt("0_0057"+tag_charg,delimiter=',',unpack='True',skip_header=1)


Uxmean3,Uymean3,Uzmean3,UxUx3,UyUy3,UzUz3,UxUy3,UxUz3,UyUz3,Ugxmean3,Ugymean3,Ugzmean3,ugp03,ugp13,ugp23,ugp33,ugp43,ugp53,UgxUgx3,UgUgMean13,UgUgMean23,UgUgMean33,UgUgy3,UgUgMean53,UgUgMean63,UgUgMean73,UgzUgz3,UlUlMean03,UlUlMean13,UlUlMean23,UlUlMean33,UlyUly3,UlUlMean53,UlUlMean63,UlUlMean73,UlzUlz3,VFav3,VFavprime3,alphaUMeanx3,alphaUMeany3,alphaUMeanz3,dummy13,dummy23,dummy33,dummy43,dummy53,dummy63,alphaprime3,alphaprimeprime3,X,Y,Z=np.genfromtxt("0_0057"+tag_charg,delimiter=',',unpack='True',skip_header=1)


VF2_average = (VFav*(t1-t0)+VFav1*(t2-t1)+VFav2*(t3-t2)+VFav3*(t4-t3))/(t4-t0)
VZ2_average = (Uymean*(t1-t0)+Uymean1*(t2-t1)+Uymean2*(t3-t2)+Uymean3*(t4-t3))/(t4-t0)
VV2_average = (UyUy*(t1-t0)+UyUy1*(t2-t1)+UyUy2*(t3-t2)+UyUy3*(t4-t3))/(t4-t0)
ViVi_av=(UyUy*(t1-t0)+UyUy1*(t2-t1)+UyUy2*(t3-t2)+UyUy3*(t4-t3))/(t4-t0)#VV2_average#-VZ2_average*VZ2_average
VX2_average = (Uzmean*(t1-t0)+Uzmean1*(t2-t1)+Uzmean2*(t3-t2)+Uzmean3*(t4-t3))/(t4-t0)
WW2_average = (UzUz*(t1-t0)+UzUz1*(t2-t1)+UzUz2*(t3-t2)+UzUz3*(t4-t3))/(t4-t0)
WiWi_av=(UzUz*(t1-t0)+UzUz1*(t2-t1)+UzUz2*(t3-t2)+UzUz3*(t4-t3))/(t4-t0)#WW2_average#-VX2_average*VX2_average
alphaw_average=(alphaUMeanz*(t1-t0)+alphaUMeanz1*(t2-t1)+alphaUMeanz2*(t3-t2)+alphaUMeanz3*(t4-t3))/(t4-t0)
alphav_average=(alphaUMeany*(t1-t0)+alphaUMeany1*(t2-t1)+alphaUMeany2*(t3-t2)+alphaUMeany3*(t4-t3))/(t4-t0)
Wg_average=(Ugzmean*(t1-t0)+Ugzmean1*(t2-t1)+Ugzmean2*(t3-t2)+Ugzmean3*(t4-t3))/((t4-t0)*(1-VF2_average))
Vg_average=(Ugymean*(t1-t0)+Ugymean1*(t2-t1)+Ugymean2*(t3-t2)+Ugymean3*(t4-t3))/((t4-t0)*(1-VF2_average))
aivi_av=alphav_average-VF2_average*VZ2_average
aiwi_av=alphaw_average-VF2_average*VZ2_average
Wli = alphaw_average/VF2_average
Vli = alphav_average/VF2_average
WgWg_average = ((UgzUgz*(t1-t0)+UgzUgz1*(t2-t1)+UgzUgz2*(t3-t2)+UgzUgz3*(t4-t3))/((t4-t0)*(1-VF2_average)))-Wg_average*Wg_average
VgVg_average = (UgUgy*(t1-t0)+UgUgy1*(t2-t1)+UgUgy2*(t3-t2)+UgUgy3*(t4-t3))/((t4-t0)*(1-VF2_average))-Vg_average*Vg_average
WlWl_average = (UlzUlz*(t1-t0)+UlzUlz1*(t2-t1)+UlzUlz2*(t3-t2)+UlzUlz3*(t4-t3))/((t4-t0)*VF2_average)-Wli*Wli
VlVl_average = (UlyUly*(t1-t0)+UlyUly1*(t2-t1)+UlyUly2*(t3-t2)+UlyUly3*(t4-t3))/((t4-t0)*VF2_average)-Vli*Vli
alphaiprime_average = (alphaprime*(t1-t0)+alphaprime1*(t2-t1)+alphaprime2*(t3-t2)+alphaprime3*(t4-t3))/(t4-t0)-VF2_average*VF2_average
UyUz_average = (UyUz*(t1-t0)+UyUz1*(t2-t1)+UyUz2*(t3-t2)+UyUz3*(t4-t3))/(t4-t0)
#---------------------------------------------------------------------------------------------------------------

DATA=VF2_average
DATA2 = ViVi_av
DATA3 = WiWi_av
DATA4 = VX2_average
DATA5 = VZ2_average
DATA6 = aivi_av
DATA7 = Wli
DATA8 = Vli
DATA9 = WgWg_average 
DATA10 = VgVg_average 
DATA11 = WlWl_average 
DATA12 = VlVl_average 
DATA13 = aiwi_av
DATA14  = Wg_average
DATA15 = Vg_average
DATA16 = alphaiprime_average
DATA17 = UyUz_average
xii = np.linspace(0, 0.004, ngridx)
yii = np.linspace(0, 0.016, ngridy)
triang = tri.Triangulation(X,Y)
interpolator = tri.LinearTriInterpolator(triang, DATA)
interpolator2 = tri.LinearTriInterpolator(triang, DATA2)
interpolator3 = tri.LinearTriInterpolator(triang, DATA3)
interpolator4 = tri.LinearTriInterpolator(triang, DATA4)
interpolator5 = tri.LinearTriInterpolator(triang, DATA5)
interpolator6 = tri.LinearTriInterpolator(triang, DATA6)
interpolator7 = tri.LinearTriInterpolator(triang, DATA7)
interpolator8 = tri.LinearTriInterpolator(triang, DATA8)
interpolator9 = tri.LinearTriInterpolator(triang, DATA9)
interpolator10 = tri.LinearTriInterpolator(triang, DATA10)
interpolator11 = tri.LinearTriInterpolator(triang, DATA11)
interpolator12 = tri.LinearTriInterpolator(triang, DATA12)
interpolator13 = tri.LinearTriInterpolator(triang, DATA13)
interpolator14 = tri.LinearTriInterpolator(triang, DATA14)
interpolator15 = tri.LinearTriInterpolator(triang, DATA15)
interpolator16 = tri.LinearTriInterpolator(triang, DATA16)
interpolator17 = tri.LinearTriInterpolator(triang, DATA17)


Xi, Yi = np.meshgrid(xii, yii)


DATAi = np.trapz(interpolator(Xi, Yi))/ngridx
DATAvvi = np.trapz(interpolator2(Xi,Yi))/ngridx
DATAwwi = np.trapz(interpolator3(Xi,Yi))/ngridx
DATAwi = np.trapz(interpolator4(Xi,Yi))/ngridx
DATAvi = np.trapz(interpolator5(Xi,Yi))/ngridx
DATAalphavi = np.trapz(interpolator6(Xi,Yi))/ngridx
DATAWl = np.trapz(interpolator7(Xi,Yi))/ngridx
DATAVl = np.trapz(interpolator8(Xi,Yi))/ngridx
DATAWgWg = np.trapz(interpolator9(Xi,Yi))/ngridx
DATAVgVg = np.trapz(interpolator10(Xi,Yi))/ngridx
DATAWlWl = np.trapz(interpolator11(Xi,Yi))/ngridx
DATAVlVl = np.trapz(interpolator12(Xi,Yi))/ngridx
DATAalphawi = np.trapz(interpolator13(Xi,Yi))/ngridx
DATAWgi = np.trapz(interpolator14(Xi,Yi))/ngridx
DATAVgi = np.trapz(interpolator15(Xi,Yi))/ngridx
DATAalphaprime = np.trapz(interpolator16(Xi,Yi))/ngridx
DATAUyUz = np.trapz(interpolator17(Xi,Yi))/ngridx

yi=yii-0.008

print("print figure")

plotVOF=DATAi
plotVV=DATAvvi
plotWW=DATAwwi
plotW=-DATAwi
plotV=DATAvi
plotalphavi=DATAalphavi
plotWl = np.nan_to_num(DATAWl) 
plotVl = np.nan_to_num(DATAVl) 
plotWgWg = np.nan_to_num(DATAWgWg) 
plotVgVg = np.nan_to_num(DATAVgVg) 
plotWlWl = np.nan_to_num(DATAWlWl) 
plotVlVl = np.nan_to_num(DATAVlVl)
plotalphawi = DATAalphawi
plotWg = np.nan_to_num(DATAWgi)
plotVg = np.nan_to_num(DATAVgi)
term1z=plotVOF*plotVlVl
term2z=(1-plotVOF)*plotVgVg
term3z=plotVOF*(1-plotVOF)*(plotVl-plotVg)**2
term1x=plotVOF*plotWlWl
term2x=(1-plotVOF)*plotWgWg
term3x=plotVOF*(1-plotVOF)*(plotWl-plotWg)**2
reconstry=plotVOF*plotVlVl+(1-plotVOF)*plotVgVg+plotVOF*(1-plotVOF)*(plotVl-plotVg)**2
reconstrz=plotVOF*plotWlWl+(1-plotVOF)*plotWgWg+plotVOF*(1-plotVOF)*(plotWl-plotWg)**2
liquid_disp=plotVOF*(1-plotVOF)
liquid_disp_true=DATAalphaprime
plotVW=DATAUyUz
####################################################


##------------------------------------------------------------------------------------------
#view_colormap('binary')
##----------- figura angolo---------------------------------
savage.save_txt('txt_integral/line_vof'+tag_txt+'_inter.txt',yi/0.001,plotVOF)
savage.save_txt('txt_integral/line_liq_disp_true'+tag_txt+'_inter.txt',yi/0.001,liquid_disp_true)
savage.save_txt('txt_integral/line_liq_disp'+tag_txt+'_inter.txt',yi/0.001,liquid_disp)
savage.save_txt('txt_integral/line_ux'+tag_txt+'_inter.txt',yi/0.001,plotW)
savage.save_txt('txt_integral/line_uz'+tag_txt+'_inter.txt',yi/0.001,plotV)
savage.save_txt('txt_integral/line_uxux'+tag_txt+'_inter.txt',yi/0.001,plotWW)
savage.save_txt('txt_integral/line_uzuz'+tag_txt+'_inter.txt',yi/0.001,plotVV)
savage.save_txt('txt_integral/line_uzux'+tag_txt+'_inter.txt',yi/0.001,plotVW)
savage.save_txt('txt_integral/line_alphav'+tag_txt+'_inter.txt',yi/0.001,plotalphavi)
savage.save_txt('txt_integral/line_alphaw'+tag_txt+'_inter.txt',yi/0.001,plotalphawi)
savage.save_txt('txt_integral/line_uxguxg'+tag_txt+'_inter.txt',yi/0.001,plotWgWg)
savage.save_txt('txt_integral/line_uxluxl'+tag_txt+'_inter.txt',yi/0.001,plotWlWl)
savage.save_txt('txt_integral/line_uzguzg'+tag_txt+'_inter.txt',yi/0.001,plotVgVg)
savage.save_txt('txt_integral/line_uzluzl'+tag_txt+'_inter.txt',yi/0.001,plotVlVl)
savage.save_txt('txt_integral/line_term1z'+tag_txt+'_inter.txt',yi/0.001,term1z)
savage.save_txt('txt_integral/line_term2z'+tag_txt+'_inter.txt',yi/0.001,term2z)
savage.save_txt('txt_integral/line_term3z'+tag_txt+'_inter.txt',yi/0.001,term3z)
savage.save_txt('txt_integral/line_term1x'+tag_txt+'_inter.txt',yi/0.001,term1x)
savage.save_txt('txt_integral/line_term2x'+tag_txt+'_inter.txt',yi/0.001,term2x)
savage.save_txt('txt_integral/line_term3x'+tag_txt+'_inter.txt',yi/0.001,term3x)


