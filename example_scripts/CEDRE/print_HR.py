#import os
#os.environ["PATH"] += os.pathsep + '/home/2017004/aremig02/latex/bin/x86_64-linux'
#import matplotlib
# Force matplotlib to not use any Xwindows backend.
#matplotlib.use('Agg')
import os
import numpy as np
import matplotlib.tri as tri
import matplotlib.cm as cm
import matplotlib.ticker

from remodarco.archer_save import archer_save as sv
from remodarco.archer_plot import archer_plot as apl
from remodarco.archer_av_paraview import archer_av_paraview as arin
from remodarco.archer_elab import archer_elab as el

import matplotlib.pyplot as plt
#---------------------------------------------------------
elab = el()
savage=sv()
drawing=apl()
ar_in=arin()
#----------------------------------------------------------

print_alpha=1
print_velocity=1
print_reynolds=1
alphav=1
#----------------------------------------------------------
class FormatScalarFormatter(matplotlib.ticker.ScalarFormatter):
        def __init__(self, fformat="%1.1f", offset=True, mathText=True):
                    self.fformat = fformat
                    matplotlib.ticker.ScalarFormatter.__init__(self,useOffset=offset,
                    useMathText=mathText)
        def _set_format(self, vmin, vmax):
                     self.format = self.fformat
                     if self._useMathText:
                      self.format = '$%s$' % matplotlib.ticker._mathdefault(self.format)
def view_colormap(cmap):
            cmap = plt.cm.get_cmap(cmap)
            colors = cmap(np.arange(cmap.N))                                
            fig, ax = plt.subplots()
            subplot_kw=dict(xticks=[], yticks=[])
            plt.imsave("gray.png",[colors])
######################################################################
dirName1='figure'
dirName2='txt'
try:
    # Create target Directory
    os.mkdir(dirName1)
    print("Directory " , dirName1 ,  " Created ")
except FileExistsError:
    print("Directory " , dirName1 ,  " already exists")

try:
    # Create target Directory
    os.mkdir(dirName2)
    print("Directory " , dirName2 ,  " Created ")
except FileExistsError:
    print("Directory " , dirName2 ,  " already exists")




# interpolation
print("interpolate")
ngridx=4000
ngridz=4000

#t0=0.00229
#t1=t0+0.0013
#t2=t1+0.00097
#t3=t2+0.001
#t4=t3+0.001
#t5=t4+0.001
#t6=t5+0.001
#t7=t6+0.001
#t8=t7+0.001
#
t0=2.29
t1=3.59
t2=4.56
t3=5.56
t4=6.56
t5=7.56
t6=8.56
t7=9.56
t8=10.56

print("interpolate first")
plotVOF,plotWg,plotVg,plotWl,plotVl,plotWgWg,plotVgVg,plotWlWl,plotVlVl,plotliqdisp=ar_in.interpolate_txt("572059_1.txt")
print("interpolate second")
plotVOF1,plotWg1,plotVg1,plotWl1,plotVl1,plotWgWg1,plotVgVg1,plotWlWl1,plotVlVl1,plotliqdisp1=ar_in.interpolate_txt("573154_1.txt")
print("interpolate third")
plotVOF2,plotWg2,plotVg2,plotWl2,plotVl2,plotWgWg2,plotVgVg2,plotWlWl2,plotVlVl2,plotliqdisp2=ar_in.interpolate_txt("574250_1.txt")
print("interpolate fourth")
plotVOF3,plotWg3,plotVg3,plotWl3,plotVl3,plotWgWg3,plotVgVg3,plotWlWl3,plotVlVl3,plotliqdisp3=ar_in.interpolate_txt("574376_1.txt")
print("interpolate fifth")
plotVOF4,plotWg4,plotVg4,plotWl4,plotVl4,plotWgWg4,plotVgVg4,plotWlWl4,plotVlVl4,plotliqdisp4=ar_in.interpolate_txt("574567_1.txt")
print("interpolate sixth")
plotVOF5,plotWg5,plotVg5,plotWl5,plotVl5,plotWgWg5,plotVgVg5,plotWlWl5,plotVlVl5,plotliqdisp5=ar_in.interpolate_txt("575006_1.txt")
print("interpolate seventh")
plotVOF6,plotWg6,plotVg6,plotWl6,plotVl6,plotWgWg6,plotVgVg6,plotWlWl6,plotVlVl6,plotliqdisp6=ar_in.interpolate_txt("575829_1.txt")
print("interpolate eigth")
plotVOF7,plotWg7,plotVg7,plotWl7,plotVl7,plotWgWg7,plotVgVg7,plotWlWl7,plotVlVl7,plotliqdisp7=ar_in.interpolate_txt("576321_1.txt")





VF2_average = (plotVOF*(t1-t0)+plotVOF1*(t2-t1)+plotVOF2*(t3-t2)+plotVOF3*(t4-t3)+plotVOF4*(t5-t4)+plotVOF5*(t6-t5)+plotVOF6*(t7-t6)+plotVOF7*(t8-t7))/(t8-t0) #(VF2*(t1-t0)+VF21*(t2-t1)+VF22*(t3-t2)+VF23*(t4-t3)+VF24*(t5-t4)+VF25*(t6-t5)+VF26*(t7-t6)+VF27*(t8-t7))/(t8-t0)
WgWg_average = (plotWgWg*(t1-t0)+plotWgWg1*(t2-t1)+plotWgWg2*(t3-t2)+plotWgWg3*(t4-t3)+plotWgWg4*(t5-t4)+plotWgWg5*(t6-t5)+plotWgWg6*(t7-t6)+plotWgWg7*(t8-t7))/(t8-t0) #(VF2*(t1-t0)+VF21*(t2-t1)+VF22*(t3-t2)+VF23*(t4-t3)+VF24*(t5-t4)+VF25*(t6-t5)+VF26*(t7-t6)+VF27*(t8-t7))/(t8-t0)
WlWl_average = (plotWlWl*(t1-t0)+plotWlWl1*(t2-t1)+plotWlWl2*(t3-t2)+plotWlWl3*(t4-t3)+plotWlWl4*(t5-t4)+plotWlWl5*(t6-t5)+plotWlWl6*(t7-t6)+plotWlWl7*(t8-t7))/(t8-t0) #(VF2*(t1-t0)+VF21*(t2-t1)+VF22*(t3-t2)+VF23*(t4-t3)+VF24*(t5-t4)+VF25*(t6-t5)+VF26*(t7-t6)+VF27*(t8-t7))/(t8-t0)
VgVg_average = (plotVgVg*(t1-t0)+plotVgVg1*(t2-t1)+plotVgVg2*(t3-t2)+plotVgVg3*(t4-t3)+plotVgVg4*(t5-t4)+plotVgVg5*(t6-t5)+plotVgVg6*(t7-t6)+plotVgVg7*(t8-t7))/(t8-t0) #(VF2*(t1-t0)+VF21*(t2-t1)+VF22*(t3-t2)+VF23*(t4-t3)+VF24*(t5-t4)+VF25*(t6-t5)+VF26*(t7-t6)+VF27*(t8-t7))/(t8-t0)
VlVl_average = (plotVlVl*(t1-t0)+plotVlVl1*(t2-t1)+plotVlVl2*(t3-t2)+plotVlVl3*(t4-t3)+plotVlVl4*(t5-t4)+plotVlVl5*(t6-t5)+plotVlVl6*(t7-t6)+plotVlVl7*(t8-t7))/(t8-t0) #(VF2*(t1-t0)+VF21*(t2-t1)+VF22*(t3-t2)+VF23*(t4-t3)+VF24*(t5-t4)+VF25*(t6-t5)+VF26*(t7-t6)+VF27*(t8-t7))/(t8-t0)
Wg_average = (plotWg*(t1-t0)+plotWg1*(t2-t1)+plotWg2*(t3-t2)+plotWg3*(t4-t3)+plotWg4*(t5-t4)+plotWg5*(t6-t5)+plotWg6*(t7-t6)+plotWg7*(t8-t7))/(t8-t0) #(VF2*(t1-t0)+VF21*(t2-t1)+VF22*(t3-t2)+VF23*(t4-t3)+VF24*(t5-t4)+VF25*(t6-t5)+VF26*(t7-t6)+VF27*(t8-t7))/(t8-t0)
Wl_average = (plotWl*(t1-t0)+plotWl1*(t2-t1)+plotWl2*(t3-t2)+plotWl3*(t4-t3)+plotWl4*(t5-t4)+plotWl5*(t6-t5)+plotWl6*(t7-t6)+plotWl7*(t8-t7))/(t8-t0) #(VF2*(t1-t0)+VF21*(t2-t1)+VF22*(t3-t2)+VF23*(t4-t3)+VF24*(t5-t4)+VF25*(t6-t5)+VF26*(t7-t6)+VF27*(t8-t7))/(t8-t0)
Vg_average = (plotVg*(t1-t0)+plotVg1*(t2-t1)+plotVg2*(t3-t2)+plotVg3*(t4-t3)+plotVg4*(t5-t4)+plotVg5*(t6-t5)+plotVg6*(t7-t6)+plotVg7*(t8-t7))/(t8-t0) #(VF2*(t1-t0)+VF21*(t2-t1)+VF22*(t3-t2)+VF23*(t4-t3)+VF24*(t5-t4)+VF25*(t6-t5)+VF26*(t7-t6)+VF27*(t8-t7))/(t8-t0)
Vl_average = (plotVl*(t1-t0)+plotVl1*(t2-t1)+plotVl2*(t3-t2)+plotVl3*(t4-t3)+plotVl4*(t5-t4)+plotVl5*(t6-t5)+plotVl6*(t7-t6)+plotVl7*(t8-t7))/(t8-t0) #(VF2*(t1-t0)+VF21*(t2-t1)+VF22*(t3-t2)+VF23*(t4-t3)+VF24*(t5-t4)+VF25*(t6-t5)+VF26*(t7-t6)+VF27*(t8-t7))/(t8-t0)

liqdisp_average = (plotliqdisp*(t1-t0)+plotliqdisp1*(t2-t1)+plotliqdisp2*(t3-t2)+plotliqdisp3*(t4-t3)+plotliqdisp4*(t5-t4)+plotliqdisp4*(t6-t5)+plotliqdisp6*(t7-t6)+plotliqdisp7*(t8-t7))/(t8-t0) #(VF2*(t1-t0)+VF21*(t2-t1)+VF22*(t3-t2)+VF23*(t4-t3)+VF24*(t5-t4)+VF25*(t6-t5)+VF26*(t7-t6)+VF27*(t8-t7))/(t8-t0)

reconstrz=VF2_average*VlVl_average+(1-VF2_average)*VgVg_average+VF2_average*(1-VF2_average)*(Vl_average-Vg_average)**2
reconstrx=VF2_average*WlWl_average+(1-VF2_average)*WgWg_average+VF2_average*(1-VF2_average)*(Wl_average-Wg_average)**2
firstz=VF2_average*VlVl_average
secondz=(1-VF2_average)*VgVg_average
thirdz=VF2_average*(1-VF2_average)*(Vl_average-Vg_average)**2
firstx=VF2_average*WlWl_average
secondx=(1-VF2_average)*WgWg_average
thirdx=VF2_average*(1-VF2_average)*(Wl_average-Wg_average)**2
liquid_disp=VF2_average*(1-VF2_average)



x1=el.get_index(ar_in.xi,0.001)
x2=el.get_index(ar_in.xi,0.002)
x3=el.get_index(ar_in.xi,0.003)



##------------------------------------------------------------------------------------------
#view_colormap('binary')
##----------- figura angolo---------------------------------
if print_alpha==1:
 residual=VF2_average[0:ngridz//2,:]-np.flip(VF2_average,0)[0:ngridz//2,:]    
 drawing.disegna_general(ar_in.xi,ar_in.zi[0:ngridz//2],residual,200,"./figure/","residual",0,0)
 drawing.disegna_vof(ar_in.xi,ar_in.zi,VF2_average,"./figure/","vof_average")
 drawing.disegna_vof_no_axis(ar_in.xi,ar_in.zi,VF2_average,"./figure/","vof_average_latex")
 drawing.disegna_no_axis(ar_in.xi,ar_in.zi,liquid_disp,200,"./figure/","liq_disp",0,0.250)
 drawing.disegna_no_axis(ar_in.xi,ar_in.zi,liqdisp_average,200,"./figure/","gliq_disp_true",0,0.250)
 liq_disp_diff = liquid_disp-liqdisp_average
 savage.save_txt('txt/line_vof_0_001_CEDRE.txt',ar_in.zi/0.001,VF2_average[:,x1])
 savage.save_txt('txt/line_vof_0_002_CEDRE.txt',ar_in.zi/0.001,VF2_average[:,x2])
 savage.save_txt('txt/line_vof_0_003_CEDRE.txt',ar_in.zi/0.001,VF2_average[:,x3])
 savage.save_txt('txt/line_liq_disp_0_001_CEDRE.txt',ar_in.zi/0.001,liquid_disp[:,x1])
 savage.save_txt('txt/line_liq_disp_0_002_CEDRE.txt',ar_in.zi/0.001,liquid_disp[:,x2])
 savage.save_txt('txt/line_liq_disp_0_003_CEDRE.txt',ar_in.zi/0.001,liquid_disp[:,x3])
 savage.save_txt('txt/line_liq_disp_true_0_001_CEDRE.txt',ar_in.zi/0.001,liqdisp_average[:,x1])
 savage.save_txt('txt/line_liq_disp_true_0_002_CEDRE.txt',ar_in.zi/0.001,liqdisp_average[:,x2])
 savage.save_txt('txt/line_liq_disp_true_0_003_CEDRE.txt',ar_in.zi/0.001,liqdisp_average[:,x3])
 savage.save_txt('txt/line_liq_disp_diff_0_001_CEDRE.txt',ar_in.zi/0.001,liq_disp_diff[:,x1])
 savage.save_txt('txt/line_liq_disp_diff_0_002_CEDRE.txt',ar_in.zi/0.001,liq_disp_diff[:,x2])
 savage.save_txt('txt/line_liq_disp_diff_0_003_CEDRE.txt',ar_in.zi/0.001,liq_disp_diff[:,x3])
if alphav==1:
 drawing.disegna_no_axis(ar_in.xi,ar_in.zi,WgWg_average,200,"./figure/","wgwg",0,260)
 drawing.disegna_no_axis(ar_in.xi,ar_in.zi,VgVg_average,200,"./figure/","vgvg",0,330)
 drawing.disegna_no_axis(ar_in.xi,ar_in.zi,VlVl_average,200,"./figure/","vlvl",0,30)
 drawing.disegna_no_axis(ar_in.xi,ar_in.zi,WlWl_average,200,"./figure/","wlwl",0,30)
 drawing.disegna_no_axis(ar_in.xi,ar_in.zi,reconstrz,200,"./figure/","reconstruct_y",0,350)
 drawing.disegna_no_axis(ar_in.xi,ar_in.zi,reconstrx,200,"./figure/","reconstruct_z",0,300)
 drawing.disegna_no_axis(ar_in.xi,ar_in.zi,firstx,200,"./figure/","firstx",0,24)
 drawing.disegna_no_axis(ar_in.xi,ar_in.zi,secondx,200,"./figure/","secondx",0,350)
 drawing.disegna_no_axis(ar_in.xi,ar_in.zi,thirdx,200,"./figure/","thirdx",0,0.001)
 drawing.disegna_no_axis(ar_in.xi,ar_in.zi,firstz,200,"./figure/","firstz",0,10)
 drawing.disegna_no_axis(ar_in.xi,ar_in.zi,secondz,200,"./figure/","secondz",0,350)
 drawing.disegna_no_axis(ar_in.xi,ar_in.zi,thirdz,200,"./figure/","thirdz",0,0.001)
 savage.save_txt('txt/line_uxguxg_0_001_cedre.txt',ar_in.zi/0.001,WgWg_average[:,x1])
 savage.save_txt('txt/line_uxguxg_0_002_cedre.txt',ar_in.zi/0.001,WgWg_average[:,x2])
 savage.save_txt('txt/line_uxguxg_0_003_cedre.txt',ar_in.zi/0.001,WgWg_average[:,x3])
 savage.save_txt('txt/line_uxluxl_0_001_cedre.txt',ar_in.zi/0.001,WlWl_average[:,x1])
 savage.save_txt('txt/line_uxluxl_0_002_cedre.txt',ar_in.zi/0.001,WlWl_average[:,x2])
 savage.save_txt('txt/line_uxluxl_0_003_cedre.txt',ar_in.zi/0.001,WlWl_average[:,x3])
 savage.save_txt('txt/line_uzguzg_0_001_cedre.txt',ar_in.zi/0.001,VgVg_average[:,x1])
 savage.save_txt('txt/line_uzguzg_0_002_cedre.txt',ar_in.zi/0.001,VgVg_average[:,x2])
 savage.save_txt('txt/line_uzguzg_0_003_cedre.txt',ar_in.zi/0.001,VgVg_average[:,x3])
 savage.save_txt('txt/line_uzluzl_0_001_cedre.txt',ar_in.zi/0.001,VlVl_average[:,x1])
 savage.save_txt('txt/line_uzluzl_0_002_cedre.txt',ar_in.zi/0.001,VlVl_average[:,x2])
 savage.save_txt('txt/line_uzluzl_0_003_cedre.txt',ar_in.zi/0.001,VlVl_average[:,x3])
 savage.save_txt('txt/line_reconstry_0_001_cedre.txt',ar_in.zi/0.001,reconstrz[:,x1])
 savage.save_txt('txt/line_reconstry_0_002_cedre.txt',ar_in.zi/0.001,reconstrz[:,x2])
 savage.save_txt('txt/line_reconstry_0_003_cedre.txt',ar_in.zi/0.001,reconstrz[:,x3])
 savage.save_txt('txt/line_reconstrz_0_001_cedre.txt',ar_in.zi/0.001,reconstrx[:,x1])
 savage.save_txt('txt/line_reconstrz_0_002_cedre.txt',ar_in.zi/0.001,reconstrx[:,x2])
 savage.save_txt('txt/line_reconstrz_0_003_cedre.txt',ar_in.zi/0.001,reconstrx[:,x3])
 savage.save_txt('txt/line_ux_diff_0_001_cedre.txt',ar_in.zi/0.001,Wl_average[:,x1]-Wg_average[:,x1])
 savage.save_txt('txt/line_ux_diff_0_002_cedre.txt',ar_in.zi/0.001,Wl_average[:,x2]-Wg_average[:,x1])
 savage.save_txt('txt/line_ux_diff_0_003_cedre.txt',ar_in.zi/0.001,Wl_average[:,x3]-Wg_average[:,x3])
 savage.save_txt('txt/line_uz_diff_0_001_cedre.txt',ar_in.zi/0.001,Vl_average[:,x1]-Vg_average[:,x1])
 savage.save_txt('txt/line_uz_diff_0_002_cedre.txt',ar_in.zi/0.001,Vl_average[:,x2]-Vg_average[:,x1])
 savage.save_txt('txt/line_uz_diff_0_003_cedre.txt',ar_in.zi/0.001,Vl_average[:,x3]-Vg_average[:,x3])
 savage.save_txt('txt/line_term1z_0_001_cedre.txt',ar_in.zi/0.001,firstz[:,x1])
 savage.save_txt('txt/line_term1z_0_002_cedre.txt',ar_in.zi/0.001,firstz[:,x2])
 savage.save_txt('txt/line_term1z_0_003_cedre.txt',ar_in.zi/0.001,firstz[:,x3])
 savage.save_txt('txt/line_term2z_0_001_cedre.txt',ar_in.zi/0.001,secondz[:,x1])
 savage.save_txt('txt/line_term2z_0_002_cedre.txt',ar_in.zi/0.001,secondz[:,x2])
 savage.save_txt('txt/line_term2z_0_003_cedre.txt',ar_in.zi/0.001,secondz[:,x3])
 savage.save_txt('txt/line_term3z_0_001_cedre.txt',ar_in.zi/0.001,thirdz[:,x1])
 savage.save_txt('txt/line_term3z_0_002_cedre.txt',ar_in.zi/0.001,thirdz[:,x2])
 savage.save_txt('txt/line_term3z_0_003_cedre.txt',ar_in.zi/0.001,thirdz[:,x3])
 savage.save_txt('txt/line_term1x_0_001_cedre.txt',ar_in.zi/0.001,firstx[:,x1])
 savage.save_txt('txt/line_term1x_0_002_cedre.txt',ar_in.zi/0.001,firstx[:,x2])
 savage.save_txt('txt/line_term1x_0_003_cedre.txt',ar_in.zi/0.001,firstx[:,x3])
 savage.save_txt('txt/line_term2x_0_001_cedre.txt',ar_in.zi/0.001,secondx[:,x1])
 savage.save_txt('txt/line_term2x_0_002_cedre.txt',ar_in.zi/0.001,secondx[:,x2])
 savage.save_txt('txt/line_term2x_0_003_cedre.txt',ar_in.zi/0.001,secondx[:,x3])
 savage.save_txt('txt/line_term3x_0_001_cedre.txt',ar_in.zi/0.001,thirdx[:,x1])
 savage.save_txt('txt/line_term3x_0_002_cedre.txt',ar_in.zi/0.001,thirdx[:,x2])
 savage.save_txt('txt/line_term3x_0_003_cedre.txt',ar_in.zi/0.001,thirdx[:,x3])



