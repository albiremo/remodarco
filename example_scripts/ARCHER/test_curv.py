#import os
#os.environ["PATH"] += os.pathsep + '/home/2017004/aremig02/latex/bin/x86_64-linux'
import matplotlib
matplotlib.use('Agg')
from remodarco.archer_save import archer_save as sv
from remodarco.archer_average import archer_average as afi
from remodarco.archer_plot import archer_plot as apl
import matplotlib.pyplot as plt
import numpy as np

savage=sv()
drawing=apl()
caso = afi("cas.3d","NAPPE_")
caso.read_case()
x = np.linspace(0, 0.004, num=caso.nx)
y = np.linspace(0, 0.016, num=caso.ny)
z = np.linspace(-0.008, 0.008, num=caso.nz) #!!!! CAMBIARE SE SI CAMBIA DIMENSIONE DEL DOMINIO E.G: DOUBLE DOMAIN!!!!#
z_shift=z+0.008
y_shift=y-0.008
flag=1
start=['00004']
caso.extract_time()
caso.catch_sequence()
windowy=0.4
windowz=0.4
caso.extract_pdf(x,y,z,start,0.004,windowy,windowz)
