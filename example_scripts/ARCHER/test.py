#import os
#os.environ["PATH"] += os.pathsep + '/home/2017004/aremig02/latex/bin/x86_64-linux'
import matplotlib
# Force matplotlib to not use any Xwindows backend.
matplotlib.use('Agg')
#from matplotlib import rc
#rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
#rc('text', usetex=True)
#rc('font', family='serif')
## for Palatino and other serif fonts use:
#rc('font',**{'family':'serif','serif':['Palatino']})
#rc('text', usetex=True)

from remodarco.archer_save import archer_save as sv
from remodarco.archer_average import archer_average as afi
from remodarco.archer_plot import archer_plot as apl
from remodarco.archer_elab import archer_elab as el
import matplotlib.pyplot as plt

import numpy as np
########### init parameters #############

print_alpha=1
print_velocity=0
print_reynolds=0
alphav=0
class FormatScalarFormatter(matplotlib.ticker.ScalarFormatter):
        def __init__(self, fformat="%1.1f", offset=True, mathText=True):
                    self.fformat = fformat
                    matplotlib.ticker.ScalarFormatter.__init__(self,useOffset=offset,
                    useMathText=mathText)
        def _set_format(self, vmin, vmax):
                     self.format = self.fformat
                     if self._useMathText:
                      self.format = '$%s$' % matplotlib.ticker._mathdefault(self.format)
def view_colormap(cmap):
            cmap = plt.cm.get_cmap(cmap)
            colors = cmap(np.arange(cmap.N))                                
            fig, ax = plt.subplots()
            subplot_kw=dict(xticks=[], yticks=[])
            plt.imsave("fig_HR/gray.png",[colors])


############ init_case #############
elab = el()
savage=sv()
drawing=apl()
caso = afi("cas.3d","NAPPE_")
caso.read_case()
x = np.linspace(0, 0.004, num=caso.nx)
y = np.linspace(0, 0.016, num=caso.ny)
z = np.linspace(-0.008, 0.008, num=caso.nz) #!!!! CAMBIARE SE SI CAMBIA DIMENSIONE DEL DOMINIO E.G: DOUBLE DOMAIN!!!!#
z_shift=z+0.008
y_shift=y-0.008
flag=1
start=['00004']
caso.extract_time()
caso.catch_sequence()

#####################################################
x1_ARCHER=el.get_index(z_shift,0.001)    
x2_ARCHER=el.get_index(z_shift,0.002)
x3_ARCHER=el.get_index(z_shift,0.003)

########### LOAD ARCHER #########"
if print_alpha==1: 
 vof_space = caso.space_average(caso.time_average(x,y,z,flag,start,"Vof_mean"))
 sign_ls = caso.space_average(caso.time_average(x,y,z,flag,start,"sign_ls"))
 corr_sign_ls = caso.space_average(caso.time_average(x,y,z,flag,start,"corr_sign_ls"))-sign_ls*sign_ls
 ld_ls=sign_ls*(1-sign_ls)
 ls_liq_disp_diff=ld_ls-corr_sign_ls
 liquid_disp_true=caso.space_average(caso.time_average(x,y,z,flag,start,"corr_ff"))-vof_space*vof_space
 liquid_disp=vof_space*(1-vof_space)
 liq_disp_diff=liquid_disp-liquid_disp_true
 convergence=vof_space[0:caso.ny//2,:]-np.flip(vof_space,0)[0:caso.ny//2,:]
 drawing.disegna_vof(z,y,vof_space,"./figure/","vof_average")
 drawing.disegna_vof_no_axis(z,y,vof_space,"./figure/","vof_average_latex")
 drawing.disegna_vof_no_axis(z,y,sign_ls,"./figure/","ls_average_latex")
 drawing.disegna_no_axis(z,y,liquid_disp,200,"./figure/","liquid_disp",0,0.250)
 drawing.disegna_no_axis(z,y,liquid_disp_true,200,"./figure/","liquid_disp_true",0,0.250)
 drawing.disegna_no_axis(z,y,ld_ls,200,"./figure/","liquid_disp_ls",0,0.250)
 drawing.disegna_no_axis(z,y,ld_ls,200,"./figure/","liquid_disp_ls_true",0,0.250)
 drawing.disegna_general(z,y[0:caso.ny//2],convergence,200,"./figure/","convergence",0,0)
 drawing.disegna_no_axis(z,y[0:caso.ny//2],convergence,200,"./figure/","convergence",0,0.05)
 savage.save_txt('txt/line_vof_ARCHER.txt',z_shift/0.001,vof_space[caso.ny//2,:])
 savage.save_txt('txt/line_vof_0_001_archer.txt',y_shift/0.001,vof_space[:,x1_ARCHER])
 savage.save_txt('txt/line_vof_0_002_archer.txt',y_shift/0.001,vof_space[:,x2_ARCHER])
 savage.save_txt('txt/line_vof_0_003_archer.txt',y_shift/0.001,vof_space[:,x3_ARCHER])
 savage.save_txt('txt/line_vof_ls_0_001_archer.txt',y_shift/0.001,sign_ls[:,x1_ARCHER])
 savage.save_txt('txt/line_vof_ls_0_002_archer.txt',y_shift/0.001,sign_ls[:,x2_ARCHER])
 savage.save_txt('txt/line_vof_ls_0_003_archer.txt',y_shift/0.001,sign_ls[:,x3_ARCHER])
 savage.save_txt('txt/line_liq_disp_0_001_archer.txt',y_shift/0.001,liquid_disp[:,x1_ARCHER])
 savage.save_txt('txt/line_liq_disp_0_002_archer.txt',y_shift/0.001,liquid_disp[:,x2_ARCHER])
 savage.save_txt('txt/line_liq_disp_0_003_archer.txt',y_shift/0.001,liquid_disp[:,x3_ARCHER])
 savage.save_txt('txt/line_liq_disp_true_0_001_archer.txt',y_shift/0.001,liquid_disp_true[:,x1_ARCHER])
 savage.save_txt('txt/line_liq_disp_true_0_002_archer.txt',y_shift/0.001,liquid_disp_true[:,x2_ARCHER])
 savage.save_txt('txt/line_liq_disp_true_0_003_archer.txt',y_shift/0.001,liquid_disp_true[:,x3_ARCHER])
 savage.save_txt('txt/line_liq_disp_diff_0_001_archer.txt',y_shift/0.001,liq_disp_diff[:,x1_ARCHER])
 savage.save_txt('txt/line_liq_disp_diff_0_002_archer.txt',y_shift/0.001,liq_disp_diff[:,x2_ARCHER])
 savage.save_txt('txt/line_liq_disp_diff_0_003_archer.txt',y_shift/0.001,liq_disp_diff[:,x3_ARCHER])
 savage.save_txt('txt/line_liq_disp_ls_0_001_archer.txt',y_shift/0.001,ld_ls[:,x1_ARCHER])
 savage.save_txt('txt/line_liq_disp_ls_0_002_archer.txt',y_shift/0.001,ld_ls[:,x2_ARCHER])
 savage.save_txt('txt/line_liq_disp_ls_0_003_archer.txt',y_shift/0.001,ld_ls[:,x3_ARCHER])
 savage.save_txt('txt/line_liq_disp_ls_true_0_001_archer.txt',y_shift/0.001,corr_sign_ls[:,x1_ARCHER])
 savage.save_txt('txt/line_liq_disp_ls_true_0_002_archer.txt',y_shift/0.001,corr_sign_ls[:,x2_ARCHER])
 savage.save_txt('txt/line_liq_disp_ls_true_0_003_archer.txt',y_shift/0.001,corr_sign_ls[:,x3_ARCHER])
 savage.save_txt('txt/line_liq_disp_diff_ls_0_001_archer.txt',y_shift/0.001,ls_liq_disp_diff[:,x1_ARCHER])
 savage.save_txt('txt/line_liq_disp_diff_ls_0_002_archer.txt',y_shift/0.001,ls_liq_disp_diff[:,x2_ARCHER])
 savage.save_txt('txt/line_liq_disp_diff_ls_0_003_archer.txt',y_shift/0.001,ls_liq_disp_diff[:,x3_ARCHER])



if print_velocity==1:
 w_space = -caso.space_average(caso.time_average(x,y,z,flag,start,"w_mean"))
 v_space = caso.space_average(caso.time_average(x,y,z,flag,start,"v_mean"))
 drawing.disegna_no_axis(z,y,w_space,200,"./figure/","w_average",0,70)
 drawing.disegna_no_axis(z,y,v_space,200,"./figure/","v_average",-3,3)
 savage.save_txt('txt/line_ux_0_001_archer.txt',y_shift/0.001,w_space[:,x1_ARCHER])
 savage.save_txt('txt/line_ux_0_002_archer.txt',y_shift/0.001,w_space[:,x2_ARCHER])
 savage.save_txt('txt/line_ux_0_003_archer.txt',y_shift/0.001,w_space[:,x3_ARCHER])
 savage.save_txt('txt/line_uz_0_001_archer.txt',y_shift/0.001,v_space[:,x1_ARCHER])
 savage.save_txt('txt/line_uz_0_002_archer.txt',y_shift/0.001,v_space[:,x2_ARCHER])
 savage.save_txt('txt/line_uz_0_003_archer.txt',y_shift/0.001,v_space[:,x3_ARCHER])
if print_reynolds==1:
 vv_space = caso.space_average(caso.time_average(x,y,z,flag,start,"vp_vp_mean"))-v_space*v_space
 ww_space = caso.space_average(caso.time_average(x,y,z,flag,start,"wp_wp_mean"))-w_space*w_space
 drawing.disegna_no_axis(z,y,vv_space,200,"./figure/","vv_average",0,300)
 drawing.disegna_no_axis(z,y,ww_space,200,"./figure/","ww_average",0,0)
 savage.save_txt('txt/line_uxux_0_001_archer.txt',y_shift/0.001,ww_space[:,x1_ARCHER])
 savage.save_txt('txt/line_uxux_0_002_archer.txt',y_shift/0.001,ww_space[:,x2_ARCHER])
 savage.save_txt('txt/line_uxux_0_003_archer.txt',y_shift/0.001,ww_space[:,x3_ARCHER])
 savage.save_txt('txt/line_uzuz_0_001_archer.txt',y_shift/0.001,vv_space[:,x1_ARCHER])
 savage.save_txt('txt/line_uzuz_0_002_archer.txt',y_shift/0.001,vv_space[:,x2_ARCHER])
 savage.save_txt('txt/line_uzuz_0_003_archer.txt',y_shift/0.001,vv_space[:,x3_ARCHER])
if alphav==1:
 alpha_vp_space = caso.space_average(caso.time_average(x,y,z,flag,start,"alpha_vp_mean"))
 alpha_wp_space = caso.space_average(caso.time_average(x,y,z,flag,start,"alpha_wp_mean"))
 vl_space = np.nan_to_num(caso.space_average(caso.time_average(x,y,z,flag,start,"alpha_vp_mean"))/vof_space)
 wl_space = -np.nan_to_num(caso.space_average(caso.time_average(x,y,z,flag,start,"alpha_wp_mean"))/vof_space)
 wg_space = -np.nan_to_num(caso.space_average(caso.time_average(x,y,z,flag,start,"wg_mean"))/(1-vof_space))
 vlvl_space = np.nan_to_num((caso.space_average(caso.time_average(x,y,z,flag,start,"vlvl_mean"))/(vof_space))-vl_space*vl_space)
 wlwl_space = np.nan_to_num((caso.space_average(caso.time_average(x,y,z,flag,start,"wlwl_mean"))/(vof_space))-wl_space*wl_space)
 vg_space = np.nan_to_num(caso.space_average(caso.time_average(x,y,z,flag,start,"vg_mean"))/(1-vof_space)) 
 vgvg_space = np.nan_to_num(caso.space_average(caso.time_average(x,y,z,flag,start,"vgvg_mean"))/(1-vof_space)-vg_space*vg_space)
 wgwg_space =np.nan_to_num(caso.space_average(caso.time_average(x,y,z,flag,start,"wgwg_mean"))/(1-vof_space)-wg_space*wg_space)
 term1z = np.nan_to_num(vof_space*vlvl_space)
 term2z = np.nan_to_num((1-vof_space)*vgvg_space)
 term3z = np.nan_to_num(vof_space*(1-vof_space)*(vl_space-vg_space)**2)
 term1x = np.nan_to_num(vof_space*wlwl_space)
 term2x = np.nan_to_num((1-vof_space)*wgwg_space)
 term3x = np.nan_to_num(vof_space*(1-vof_space)*(wl_space-wg_space)**2)


 reconstruct_y=vof_space*vlvl_space+(1-vof_space)*vgvg_space+vof_space*(1-vof_space)*(vl_space-vg_space)**2
 reconstruct_z=vof_space*wlwl_space+(1-vof_space)*wgwg_space+vof_space*(1-vof_space)*(wl_space-wg_space)**2

 drawing.disegna_general(z,y,alpha_vp_space,200,"./figure/","alpha_vp_average",-1.5,1.5) 
 drawing.disegna_general(z,y,alpha_wp_space,200,"./figure/","alpha_wp_average",0,0)
 drawing.disegna_general(z,y,vl_space-vg_space,200,"./figure/","v_diff_average",0,0)
 drawing.disegna_general(z,y,wl_space-wg_space,200,"./figure/","w_diff_average",0,0)
 drawing.disegna_no_axis(z,y,vl_space,200,"./figure/","vl_average",0,0)
 drawing.disegna_no_axis(z,y,wl_space,200,"./figure/","wl_average",0,75)
 drawing.disegna_no_axis(z,y,wg_space,200,"./figure/","wg_average",0,80)
 drawing.disegna_no_axis(z,y,vg_space,200,"./figure/","vg_average",-6,6)
 drawing.disegna_no_axis(z,y,vlvl_space,200,"./figure/","vlvl_average",0,30)
 drawing.disegna_no_axis(z,y,wlwl_space,200,"./figure/","wlwl_average",0,30)
 drawing.disegna_no_axis(z,y,wgwg_space,200,"./figure/","wgwg_average",0,260)
 drawing.disegna_no_axis(z,y,vgvg_space,200,"./figure/","vgvg_average",0,330)
 drawing.disegna_no_axis(z,y,reconstruct_y,200,"./figure/","reconstruct_y",0,0)
 drawing.disegna_no_axis(z,y,reconstruct_z,200,"./figure/","reconstruct_z",0,0)
 drawing.disegna_no_axis(z,y,term1z,200,"./figure/","term1z",0,3)
 drawing.disegna_no_axis(z,y,term2z,200,"./figure/","term2z",0,350)
 drawing.disegna_no_axis(z,y,term3z,200,"./figure/","term3z",0,4)
 drawing.disegna_no_axis(z,y,term1x,200,"./figure/","term1x",0,2.8)
 drawing.disegna_no_axis(z,y,term2x,200,"./figure/","term2x",0,350)
 drawing.disegna_no_axis(z,y,term3x,200,"./figure/","term3x",0,70)
#draw
 savage.save_txt('txt/line_alphav_0_001_archer.txt',y_shift/0.001,alpha_vp_space[:,x1_ARCHER])
 savage.save_txt('txt/line_alphav_0_002_archer.txt',y_shift/0.001,alpha_vp_space[:,x2_ARCHER])
 savage.save_txt('txt/line_alphav_0_003_archer.txt',y_shift/0.001,alpha_vp_space[:,x3_ARCHER])
 savage.save_txt('txt/line_uxguxg_0_001_archer.txt',y_shift/0.001,wgwg_space[:,x1_ARCHER])
 savage.save_txt('txt/line_uxguxg_0_002_archer.txt',y_shift/0.001,wgwg_space[:,x2_ARCHER])
 savage.save_txt('txt/line_uxguxg_0_003_archer.txt',y_shift/0.001,wgwg_space[:,x3_ARCHER])
 savage.save_txt('txt/line_uxluxl_0_001_archer.txt',y_shift/0.001,wlwl_space[:,x1_ARCHER])
 savage.save_txt('txt/line_uxluxl_0_002_archer.txt',y_shift/0.001,wlwl_space[:,x2_ARCHER])
 savage.save_txt('txt/line_uxluxl_0_003_archer.txt',y_shift/0.001,wlwl_space[:,x3_ARCHER])
 savage.save_txt('txt/line_uyguyg_0_001_archer.txt',y_shift/0.001,vgvg_space[:,x1_ARCHER])
 savage.save_txt('txt/line_uyguyg_0_002_archer.txt',y_shift/0.001,vgvg_space[:,x2_ARCHER])
 savage.save_txt('txt/line_uyguyg_0_003_archer.txt',y_shift/0.001,vgvg_space[:,x3_ARCHER])
 savage.save_txt('txt/line_uyluyl_0_001_archer.txt',y_shift/0.001,vlvl_space[:,x1_ARCHER])
 savage.save_txt('txt/line_uyluyl_0_002_archer.txt',y_shift/0.001,vlvl_space[:,x2_ARCHER])
 savage.save_txt('txt/line_uyluyl_0_003_archer.txt',y_shift/0.001,vlvl_space[:,x3_ARCHER])
 savage.save_txt('txt/line_reconstructy_0_001_archer.txt',y_shift/0.001,reconstruct_y[:,x1_ARCHER])
 savage.save_txt('txt/line_reconstructy_0_002_archer.txt',y_shift/0.001,reconstruct_y[:,x2_ARCHER])
 savage.save_txt('txt/line_reconstructy_0_003_archer.txt',y_shift/0.001,reconstruct_y[:,x3_ARCHER])
 savage.save_txt('txt/line_reconstructz_0_001_archer.txt',y_shift/0.001,reconstruct_z[:,x1_ARCHER])
 savage.save_txt('txt/line_reconstructz_0_002_archer.txt',y_shift/0.001,reconstruct_z[:,x2_ARCHER])
 savage.save_txt('txt/line_reconstructz_0_003_archer.txt',y_shift/0.001,reconstruct_z[:,x3_ARCHER])
 savage.save_txt('txt/line_uz_diff_0_001_archer.txt',y_shift/0.001,vl_space[:,x1_ARCHER]-vg_space[:,x1_ARCHER])
 savage.save_txt('txt/line_uz_diff_0_002_archer.txt',y_shift/0.001,vl_space[:,x2_ARCHER]-vg_space[:,x2_ARCHER])
 savage.save_txt('txt/line_uz_diff_0_003_archer.txt',y_shift/0.001,vl_space[:,x3_ARCHER]-vg_space[:,x3_ARCHER])
 savage.save_txt('txt/line_ux_diff_0_001_archer.txt',y_shift/0.001,wl_space[:,x1_ARCHER]-wg_space[:,x1_ARCHER])
 savage.save_txt('txt/line_ux_diff_0_002_archer.txt',y_shift/0.001,wl_space[:,x2_ARCHER]-wg_space[:,x2_ARCHER])
 savage.save_txt('txt/line_ux_diff_0_003_archer.txt',y_shift/0.001,wl_space[:,x3_ARCHER]-wg_space[:,x3_ARCHER])
 savage.save_txt('txt/line_term1x_0_001_archer.txt',y_shift/0.001,term1x[:,x1_ARCHER])
 savage.save_txt('txt/line_term1x_0_002_archer.txt',y_shift/0.001,term1x[:,x2_ARCHER])
 savage.save_txt('txt/line_term1x_0_003_archer.txt',y_shift/0.001,term1x[:,x3_ARCHER])
 savage.save_txt('txt/line_term2x_0_001_archer.txt',y_shift/0.001,term2x[:,x1_ARCHER])
 savage.save_txt('txt/line_term2x_0_002_archer.txt',y_shift/0.001,term2x[:,x2_ARCHER])
 savage.save_txt('txt/line_term2x_0_003_archer.txt',y_shift/0.001,term2x[:,x3_ARCHER])
 savage.save_txt('txt/line_term3x_0_001_archer.txt',y_shift/0.001,term3x[:,x1_ARCHER])
 savage.save_txt('txt/line_term3x_0_002_archer.txt',y_shift/0.001,term3x[:,x2_ARCHER])
 savage.save_txt('txt/line_term3x_0_003_archer.txt',y_shift/0.001,term3x[:,x3_ARCHER])
 savage.save_txt('txt/line_term1z_0_001_archer.txt',y_shift/0.001,term1z[:,x1_ARCHER])
 savage.save_txt('txt/line_term1z_0_002_archer.txt',y_shift/0.001,term1z[:,x2_ARCHER])
 savage.save_txt('txt/line_term1z_0_003_archer.txt',y_shift/0.001,term1z[:,x3_ARCHER])
 savage.save_txt('txt/line_term2z_0_001_archer.txt',y_shift/0.001,term2z[:,x1_ARCHER])
 savage.save_txt('txt/line_term2z_0_002_archer.txt',y_shift/0.001,term2z[:,x2_ARCHER])
 savage.save_txt('txt/line_term2z_0_003_archer.txt',y_shift/0.001,term2z[:,x3_ARCHER])
 savage.save_txt('txt/line_term3z_0_001_archer.txt',y_shift/0.001,term3z[:,x1_ARCHER])
 savage.save_txt('txt/line_term3z_0_002_archer.txt',y_shift/0.001,term3z[:,x2_ARCHER])
 savage.save_txt('txt/line_term3z_0_003_archer.txt',y_shift/0.001,term3z[:,x3_ARCHER])











