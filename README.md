# REMODARCO
This is a fast OOP library to postprocess *ARCHER* file. In particular it has been tested on a *COUCHE* configuration (**IN PROGRESS**: test on other configurations like the collision test)
It is composed by two different parts:
- *archer_file.py*: all the routines and objects related to the reading and printing of the file;
- *archer_elab.py*: all the routines related to the elaboration of data

## archer_file.py

In this class it is possible to find all the routines related to the reading and writing of the files originated by a simulation in ARCHER.

### read_case

In this routine is read the case file of the simulation to gather the following information:
- number of nodes in x,y,z
- nummber of procs in x,y,z

the output are given as attribute of the class
### extract_time

In this routine, through the elaboration of the xmf file, a sorted list of file is given. A file *time_list.txt* is written with sorted time saved and a *self.time* vector is given as an attribute to the class.\s
**WARNING!** In the time list there are not double time. The relaunch time is already free.

### catch_sequence

Routine built in order to obtain two ordered vectors (*relaunch* and *iteraz*) with all the relaunch and iteraz necessary.\s
e.g:\s
NAPPE_00002_st000024.xmf\s
relaunch = 00002\s
iteraz = st000024\s
In this function, the restart file are automatically deleted, so it is already a clean vector. The *relaunch* and *iteraz* vectors can be given both as results of the computation or as a method.

### read_h5
This group of routines read the given field subdivided in the different processors and return a vector of dimensions *(nx,ny,nz)*. It is possible to add other post processing fields by simply copying the strycture of the function.


### print_vof

